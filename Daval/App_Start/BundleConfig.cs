﻿using System.Web;
using System.Web.Optimization;

namespace Daval
{
    public class BundleConfig
    {
        // Para obtener más información acerca de Bundling, consulte http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información sobre los formularios. De este modo, estará
            // preparado para la producción y podrá utilizar la herramienta de creación disponible en http://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/style").Include(
                "~/MetroKing/style/bootstrap.css",
                "~/MetroKing/style/font-awesome.css",
                "~/MetroKing/style/jquery-ui.css",
                "~/MetroKing/style/fullcalendar.css",
                "~/MetroKing/style/bootstrap-datetimepicker.min.css",
                "~/MetroKing/style/bootstrap-switch.css",
                "~/MetroKing/style/style.css",
                "~/MetroKing/style/widgets.css"
                ));

            /***********************************************************************/
             
            /***********************************************************************/
           //<link href="~/MetroKing/style/prettyPhoto.css" rel="stylesheet" />
           //<link href="~/MetroKing/style/rateit.css" rel="stylesheet" />
            /***********************************************************************/
            
           // /***********************************************************************/
           //<link href="~/MetroKing/style/jquery.gritter.css" rel="stylesheet" />
           // <link href="~/MetroKing/style/jquery.cleditor.css" rel="stylesheet" />
            /***********************************************************************/
            

            bundles.Add(new ScriptBundle("~/Content/js").Include(
                "~/MetroKing/js/bootstrap.js",
                "~/MetroKing/js/fullcalendar.min.js",
                "~/MetroKing/js/bootstrap-datetimepicker.min.js",
                "~/MetroKing/js/bootstrap-switch.min.js",
                "~/MetroKing/js/custom.js"
                ));

            bundles.Add(new ScriptBundle("~/Content/Sparklines").Include(
                "~/MetroKing/js/sparklines.js"
                //"~/MetroKing/js/charts.js"
                ));

            /***********************************************************************/

            /***********************************************************************/
            //<script src="~/MetroKing/js/jquery.rateit.min.js"></script>
            //<script src="~/MetroKing/js/jquery.prettyPhoto.js"></script>
            //<script src="~/MetroKing/js/excanvas.min.js"></script>
            //<script src="~/MetroKing/js/jquery.flot.js"></script>
            //<script src="~/MetroKing/js/jquery.flot.resize.js"></script>
            //<script src="~/MetroKing/js/jquery.flot.pie.js"></script>
            //<script src="~/MetroKing/js/jquery.flot.stack.js"></script>
            //<script src="~/MetroKing/js/jquery.gritter.min.js"></script>
            //<script src="~/MetroKing/js/sparklines.js"></script>
            //<script src="~/MetroKing/js/jquery.cleditor.min.js"></script>
            /***********************************************************************/

            /***********************************************************************/
            //<script src="~/MetroKing/js/filter.js"></script>
            /***********************************************************************/

        }
    }
}