namespace Daval.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _25Nov : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Estado",
                c => new
                    {
                        idEstado = c.Int(nullable: false, identity: true),
                        descripcion = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.idEstado);
            
            CreateTable(
                "dbo.Usuario",
                c => new
                    {
                        idUsuario = c.Int(nullable: false, identity: true),
                        apellido = c.String(nullable: false, maxLength: 30),
                        nombre = c.String(nullable: false, maxLength: 30),
                        usuario = c.String(nullable: false, maxLength: 10),
                        contraseña = c.String(nullable: false, maxLength: 10),
                        idRol = c.Int(nullable: false),
                        idEstado = c.Int(nullable: false),
                        passActual = c.String(),
                        passNueva = c.String(maxLength: 10),
                        confirmarPass = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.idUsuario)
                .ForeignKey("dbo.Rol", t => t.idRol, cascadeDelete: true)
                .ForeignKey("dbo.Estado", t => t.idEstado)
                .Index(t => t.idRol)
                .Index(t => t.idEstado);
            
            CreateTable(
                "dbo.Rol",
                c => new
                    {
                        idRol = c.Int(nullable: false, identity: true),
                        rolName = c.String(nullable: false, maxLength: 30),
                    })
                .PrimaryKey(t => t.idRol);
            
            CreateTable(
                "dbo.Medidor",
                c => new
                    {
                        idMedidor = c.Int(nullable: false, identity: true),
                        serial = c.String(nullable: false),
                        lote = c.String(nullable: false),
                        fecha_entrada = c.DateTime(nullable: false),
                        fecha_salida = c.DateTime(),
                        mca = c.Single(nullable: false),
                        mcc = c.Single(nullable: false),
                        nivelBateria = c.Single(nullable: false),
                        idEstado = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idMedidor)
                .ForeignKey("dbo.Estado", t => t.idEstado)
                .Index(t => t.idEstado);
            
            CreateTable(
                "dbo.Tarjeta",
                c => new
                    {
                        idTarjeta = c.Int(nullable: false, identity: true),
                        serial = c.String(nullable: false),
                        lote = c.String(nullable: false),
                        contraseña = c.String(nullable: false, maxLength: 10),
                        fecha_entrada = c.DateTime(nullable: false),
                        fecha_salida = c.DateTime(),
                        idEstado = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idTarjeta)
                .ForeignKey("dbo.Estado", t => t.idEstado)
                .Index(t => t.idEstado);
            
            CreateTable(
                "dbo.Recarga",
                c => new
                    {
                        idRecarga = c.Guid(nullable: false, identity: true),
                        mcc = c.Single(nullable: false),
                        valor = c.Single(nullable: false),
                        fecRecarga = c.DateTime(nullable: false),
                        idTarjeta = c.String(),
                        idUsuario = c.Int(nullable: false),
                        idPos = c.Int(nullable: false),
                        idEstado = c.Int(nullable: false),
                        tarjeta_idTarjeta = c.Int(),
                    })
                .PrimaryKey(t => t.idRecarga)
                .ForeignKey("dbo.Tarjeta", t => t.tarjeta_idTarjeta)
                .ForeignKey("dbo.Usuario", t => t.idUsuario, cascadeDelete: true)
                .ForeignKey("dbo.POS", t => t.idPos, cascadeDelete: true)
                .ForeignKey("dbo.Estado", t => t.idEstado)
                .Index(t => t.tarjeta_idTarjeta)
                .Index(t => t.idUsuario)
                .Index(t => t.idPos)
                .Index(t => t.idEstado);
            
            CreateTable(
                "dbo.POS",
                c => new
                    {
                        idPOS = c.Int(nullable: false, identity: true),
                        nombre = c.String(nullable: false, maxLength: 30),
                        dirección = c.String(nullable: false, maxLength: 50),
                        fecha_reg = c.DateTime(nullable: false),
                        idEstado = c.Int(nullable: false),
                        token = c.String(),
                    })
                .PrimaryKey(t => t.idPOS)
                .ForeignKey("dbo.Estado", t => t.idEstado)
                .Index(t => t.idEstado);
            
            CreateTable(
                "dbo.Estrato",
                c => new
                    {
                        idEstrato = c.Int(nullable: false, identity: true),
                        estratoNom = c.String(nullable: false, maxLength: 20),
                        valor = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idEstrato);
            
            CreateTable(
                "dbo.Opcion",
                c => new
                    {
                        idOpcion = c.Int(nullable: false),
                        nombreOpcion = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.idOpcion);
            
            CreateTable(
                "dbo.OpcionRol",
                c => new
                    {
                        idRol = c.Int(nullable: false),
                        idOpcion = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.idRol, t.idOpcion })
                .ForeignKey("dbo.Rol", t => t.idRol, cascadeDelete: true)
                .ForeignKey("dbo.Opcion", t => t.idOpcion, cascadeDelete: true)
                .Index(t => t.idRol)
                .Index(t => t.idOpcion);
            
            CreateTable(
                "dbo.Municipio",
                c => new
                    {
                        idMunicipio = c.Int(nullable: false, identity: true),
                        nombre = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.idMunicipio);
            
            CreateTable(
                "dbo.Cliente",
                c => new
                    {
                        cedula = c.String(nullable: false, maxLength: 128),
                        apellido = c.String(nullable: false, maxLength: 30),
                        nombre = c.String(nullable: false, maxLength: 30),
                        direccion = c.String(nullable: false, maxLength: 30),
                        telefono = c.String(nullable: false, maxLength: 10),
                        idMunicipio = c.Int(nullable: false),
                        idEstrato = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.cedula)
                .ForeignKey("dbo.Estrato", t => t.idEstrato, cascadeDelete: true)
                .ForeignKey("dbo.Municipio", t => t.idMunicipio, cascadeDelete: true)
                .Index(t => t.idEstrato)
                .Index(t => t.idMunicipio);
            
            CreateTable(
                "dbo.TipoAlerta",
                c => new
                    {
                        idTipoAlerta = c.Int(nullable: false, identity: true),
                        descripcion = c.String(nullable: false, maxLength: 30),
                    })
                .PrimaryKey(t => t.idTipoAlerta);
            
            CreateTable(
                "dbo.TipoError",
                c => new
                    {
                        idTipoError = c.Int(nullable: false, identity: true),
                        descripcion = c.String(nullable: false, maxLength: 30),
                    })
                .PrimaryKey(t => t.idTipoError);
            
            CreateTable(
                "dbo.Alerta",
                c => new
                    {
                        idAlerta = c.Int(nullable: false, identity: true),
                        idMedidor = c.Int(nullable: false),
                        idTipoAlerta = c.Int(nullable: false),
                        fecha = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.idAlerta)
                .ForeignKey("dbo.Medidor", t => t.idMedidor, cascadeDelete: true)
                .ForeignKey("dbo.TipoAlerta", t => t.idTipoAlerta, cascadeDelete: true)
                .Index(t => t.idMedidor)
                .Index(t => t.idTipoAlerta);
            
            CreateTable(
                "dbo.Error",
                c => new
                    {
                        idError = c.Int(nullable: false, identity: true),
                        idPOS = c.Int(nullable: false),
                        idTipoError = c.Int(nullable: false),
                        fecha = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.idError)
                .ForeignKey("dbo.POS", t => t.idPOS, cascadeDelete: true)
                .ForeignKey("dbo.TipoError", t => t.idTipoError, cascadeDelete: true)
                .Index(t => t.idPOS)
                .Index(t => t.idTipoError);
            
            CreateTable(
                "dbo.POA",
                c => new
                    {
                        idPOA = c.Int(nullable: false, identity: true),
                        nombre = c.String(nullable: false, maxLength: 20),
                        dirección = c.String(nullable: false, maxLength: 50),
                        fecha_reg = c.DateTime(nullable: false),
                        idEstado = c.Int(nullable: false),
                        token = c.String(),
                    })
                .PrimaryKey(t => t.idPOA)
                .ForeignKey("dbo.Estado", t => t.idEstado)
                .Index(t => t.idEstado);
            
            CreateTable(
                "dbo.ClienteMedidor",
                c => new
                    {
                        Cliente = c.String(nullable: false, maxLength: 128),
                        Medidor = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Cliente, t.Medidor })
                .ForeignKey("dbo.Cliente", t => t.Cliente, cascadeDelete: true)
                .ForeignKey("dbo.Medidor", t => t.Medidor, cascadeDelete: true)
                .Index(t => t.Cliente)
                .Index(t => t.Medidor);
            
            CreateTable(
                "dbo.ClienteTarjeta",
                c => new
                    {
                        Cliente = c.String(nullable: false, maxLength: 128),
                        Tarjeta = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Cliente, t.Tarjeta })
                .ForeignKey("dbo.Cliente", t => t.Cliente, cascadeDelete: true)
                .ForeignKey("dbo.Tarjeta", t => t.Tarjeta, cascadeDelete: true)
                .Index(t => t.Cliente)
                .Index(t => t.Tarjeta);
            
            CreateTable(
                "dbo.HistorialSesion",
                c => new
                    {
                        idHistSesion = c.Int(nullable: false, identity: true),
                        idUsuario = c.Int(nullable: false),
                        idEstado = c.Int(nullable: false),
                        fechaInicio = c.DateTime(nullable: false),
                        fechaFin = c.DateTime(nullable: false),
                        ip = c.String(),
                        token = c.String(),
                    })
                .PrimaryKey(t => t.idHistSesion)
                .ForeignKey("dbo.Usuario", t => t.idUsuario, cascadeDelete: true)
                .ForeignKey("dbo.Estado", t => t.idEstado)
                .Index(t => t.idUsuario)
                .Index(t => t.idEstado);
            
            CreateTable(
                "dbo.Configuraciones",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        recargaEmergencia = c.Int(nullable: false),
                        intervaloCopiaSeguridad = c.Int(nullable: false),
                        intervaloNotificacion = c.Int(nullable: false),
                        intervaloMantenimiento = c.Int(nullable: false),
                        intervaloSesión = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.HistorialSesion", new[] { "idEstado" });
            DropIndex("dbo.HistorialSesion", new[] { "idUsuario" });
            DropIndex("dbo.ClienteTarjeta", new[] { "Tarjeta" });
            DropIndex("dbo.ClienteTarjeta", new[] { "Cliente" });
            DropIndex("dbo.ClienteMedidor", new[] { "Medidor" });
            DropIndex("dbo.ClienteMedidor", new[] { "Cliente" });
            DropIndex("dbo.POA", new[] { "idEstado" });
            DropIndex("dbo.Error", new[] { "idTipoError" });
            DropIndex("dbo.Error", new[] { "idPOS" });
            DropIndex("dbo.Alerta", new[] { "idTipoAlerta" });
            DropIndex("dbo.Alerta", new[] { "idMedidor" });
            DropIndex("dbo.Cliente", new[] { "idMunicipio" });
            DropIndex("dbo.Cliente", new[] { "idEstrato" });
            DropIndex("dbo.OpcionRol", new[] { "idOpcion" });
            DropIndex("dbo.OpcionRol", new[] { "idRol" });
            DropIndex("dbo.POS", new[] { "idEstado" });
            DropIndex("dbo.Recarga", new[] { "idEstado" });
            DropIndex("dbo.Recarga", new[] { "idPos" });
            DropIndex("dbo.Recarga", new[] { "idUsuario" });
            DropIndex("dbo.Recarga", new[] { "tarjeta_idTarjeta" });
            DropIndex("dbo.Tarjeta", new[] { "idEstado" });
            DropIndex("dbo.Medidor", new[] { "idEstado" });
            DropIndex("dbo.Usuario", new[] { "idEstado" });
            DropIndex("dbo.Usuario", new[] { "idRol" });
            DropForeignKey("dbo.HistorialSesion", "idEstado", "dbo.Estado");
            DropForeignKey("dbo.HistorialSesion", "idUsuario", "dbo.Usuario");
            DropForeignKey("dbo.ClienteTarjeta", "Tarjeta", "dbo.Tarjeta");
            DropForeignKey("dbo.ClienteTarjeta", "Cliente", "dbo.Cliente");
            DropForeignKey("dbo.ClienteMedidor", "Medidor", "dbo.Medidor");
            DropForeignKey("dbo.ClienteMedidor", "Cliente", "dbo.Cliente");
            DropForeignKey("dbo.POA", "idEstado", "dbo.Estado");
            DropForeignKey("dbo.Error", "idTipoError", "dbo.TipoError");
            DropForeignKey("dbo.Error", "idPOS", "dbo.POS");
            DropForeignKey("dbo.Alerta", "idTipoAlerta", "dbo.TipoAlerta");
            DropForeignKey("dbo.Alerta", "idMedidor", "dbo.Medidor");
            DropForeignKey("dbo.Cliente", "idMunicipio", "dbo.Municipio");
            DropForeignKey("dbo.Cliente", "idEstrato", "dbo.Estrato");
            DropForeignKey("dbo.OpcionRol", "idOpcion", "dbo.Opcion");
            DropForeignKey("dbo.OpcionRol", "idRol", "dbo.Rol");
            DropForeignKey("dbo.POS", "idEstado", "dbo.Estado");
            DropForeignKey("dbo.Recarga", "idEstado", "dbo.Estado");
            DropForeignKey("dbo.Recarga", "idPos", "dbo.POS");
            DropForeignKey("dbo.Recarga", "idUsuario", "dbo.Usuario");
            DropForeignKey("dbo.Recarga", "tarjeta_idTarjeta", "dbo.Tarjeta");
            DropForeignKey("dbo.Tarjeta", "idEstado", "dbo.Estado");
            DropForeignKey("dbo.Medidor", "idEstado", "dbo.Estado");
            DropForeignKey("dbo.Usuario", "idEstado", "dbo.Estado");
            DropForeignKey("dbo.Usuario", "idRol", "dbo.Rol");
            DropTable("dbo.Configuraciones");
            DropTable("dbo.HistorialSesion");
            DropTable("dbo.ClienteTarjeta");
            DropTable("dbo.ClienteMedidor");
            DropTable("dbo.POA");
            DropTable("dbo.Error");
            DropTable("dbo.Alerta");
            DropTable("dbo.TipoError");
            DropTable("dbo.TipoAlerta");
            DropTable("dbo.Cliente");
            DropTable("dbo.Municipio");
            DropTable("dbo.OpcionRol");
            DropTable("dbo.Opcion");
            DropTable("dbo.Estrato");
            DropTable("dbo.POS");
            DropTable("dbo.Recarga");
            DropTable("dbo.Tarjeta");
            DropTable("dbo.Medidor");
            DropTable("dbo.Rol");
            DropTable("dbo.Usuario");
            DropTable("dbo.Estado");
        }
    }
}
