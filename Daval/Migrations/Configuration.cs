namespace Daval.Migrations
{
    using Daval.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Daval.Models.DavalDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Daval.Models.DavalDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            var dataMunicipios = new List<Municipio>
            {
                new Municipio { idMunicipio = 1, nombre = "Medell�n"},
                new Municipio { idMunicipio = 2, nombre = "Bello"},
                new Municipio { idMunicipio = 3, nombre = "Itag��"}
            };
            dataMunicipios.ForEach(s => context.municipios.Add(s));
            context.SaveChanges();

            //-------------------------------------------------------------------------------

            var dataEstratos = new List<Estrato> {
                new Estrato { idEstrato = 1, estratoNom = "Uno", valor = 20},
                new Estrato { idEstrato = 2, estratoNom = "Dos", valor = 40}
            };
            dataEstratos.ForEach(s => context.estratos.Add(s));
            context.SaveChanges();

            //-------------------------------------------------------------------------------

            var dataEstados = new List<Estado>
            {
                new Estado {idEstado = 1 , descripcion = "En linea"},
                new Estado {idEstado = 2 , descripcion= "Desconectado"},
                new Estado {idEstado = 3 , descripcion= "Activo"},
                new Estado {idEstado = 4 , descripcion= "Inactivo"},
                new Estado {idEstado = 5 , descripcion= "En bodega"},
                new Estado {idEstado = 6 , descripcion= "Da�ado"},
                new Estado {idEstado = 7 , descripcion= "En Mantenimiento"},
                new Estado {idEstado = 8 , descripcion= "Fuera de servicio"},
                new Estado {idEstado = 9 , descripcion= "En Proceso"}
            };
            dataEstados.ForEach(s => context.estados.Add(s));
            context.SaveChanges();

            //---------------------------------------------------------------------------------------

            var dataRoles = new List<Rol>
            {
                new Rol {idRol = 1 , rolName ="Superusuario"},
                new Rol {idRol = 2 , rolName ="Supervisor"},
                new Rol {idRol = 3, rolName ="Tecnico de soporte"},
                new Rol {idRol = 4 , rolName ="Recaudador"},
                new Rol {idRol = 5 , rolName ="Inventario"},
                new Rol {idRol = 6 , rolName ="Operario"}
            };
            dataRoles.ForEach(s => context.roles.Add(s));
            context.SaveChanges();

            //---------------------------------------------------------------------------------

            var dataTipoAlerta = new List<TipoAlerta>
            {
                new TipoAlerta {idTipoAlerta = 1, descripcion = "incidentes mageneticos"},
                new TipoAlerta {idTipoAlerta = 2, descripcion = "incidentes bateria baja"},
                new TipoAlerta {idTipoAlerta = 3, descripcion = "apertura del contador"},
                new TipoAlerta {idTipoAlerta = 4, descripcion = "reinicio"},
                new TipoAlerta {idTipoAlerta = 5, descripcion = "error al cerrar la v�lvula"},
                new TipoAlerta {idTipoAlerta = 6, descripcion = "error al abrir la v�lvula"},
                new TipoAlerta {idTipoAlerta = 7, descripcion = "error al leer tarjeta"}
                
            };
            dataTipoAlerta.ForEach(s => context.tipoAlertas.Add(s));
            context.SaveChanges();

            //-------------------------------------------------------------------------------------

            var dataTipoError = new List<TipoError>
            {
                new TipoError {idTipoError = 1 , descripcion = "incidente seguridad"},
                new TipoError {idTipoError = 2 , descripcion = "POS sin conexion"},
                new TipoError {idTipoError = 3 , descripcion = "Caida del sistema"},
                new TipoError {idTipoError = 4 , descripcion = "Doble sesion"},
                new TipoError {idTipoError = 5 , descripcion = "Recarga fallida"}
            };
            dataTipoError.ForEach(s => context.tipoError.Add(s));
            context.SaveChanges();

            //-------------------------------------------------------------------------------------

            var dataOpciones = new List<Opcion>
            {
                new Opcion { idOpcion = 0 , nombreOpcion = "Panel de Control"},
                new Opcion { idOpcion = 1 , nombreOpcion = "Gesti�n de Configuraciones"},
                new Opcion { idOpcion = 100 , nombreOpcion = "Reportes"},
                new Opcion { idOpcion = 101	, nombreOpcion = "Historial de Recargas"},
                new Opcion { idOpcion = 102 , nombreOpcion = "Historial de Alertas"},
                new Opcion { idOpcion = 103	, nombreOpcion = "Historial de Errores"},
                new Opcion { idOpcion = 104 , nombreOpcion = "Relaci�n de Clientes y Medidores"},
                new Opcion { idOpcion = 105 , nombreOpcion = "Relaci�n de Clientes y Tarjetas"},
                new Opcion { idOpcion = 106 , nombreOpcion = "Relaci�n de Recargas y Puntos de Venta"},
                new Opcion { idOpcion = 200 , nombreOpcion = "Gesti�n del Servicio"},
                new Opcion { idOpcion = 201 , nombreOpcion = "Gesti�n de Municipios"},
                new Opcion { idOpcion = 202	, nombreOpcion = "Gesti�n de Estratos"},
                new Opcion { idOpcion = 203 , nombreOpcion = "Gesti�n de Puntos de Venta"},
                new Opcion { idOpcion = 204 , nombreOpcion = "Gesti�n de Puntos de Administraci�n"},
                new Opcion { idOpcion = 205	, nombreOpcion = "Gesti�n de Medidores"},
                new Opcion { idOpcion = 206	, nombreOpcion = "Gesti�n de Tarjetas"},
                new Opcion { idOpcion = 207	, nombreOpcion = "Gesti�n de Clientes"},
                new Opcion { idOpcion = 300	, nombreOpcion = "Gesti�n de Seguridad"},
                new Opcion { idOpcion = 301	, nombreOpcion = "Gesti�n de Estados"},
                new Opcion { idOpcion = 302	, nombreOpcion = "Gesti�n de Roles"},
                new Opcion { idOpcion = 303 , nombreOpcion = "Gesti�n de Usuarios"}
            };
            dataOpciones.ForEach(s => context.opciones.Add(s));
            context.SaveChanges();

            //-------------------------------------------------------------------------------------

            var dataOpcionesRol = new List<OpcionRol>
            {
                new OpcionRol { idOpcion = 0 , idRol = 1 },
                new OpcionRol { idOpcion = 1 , idRol = 1 },
                new OpcionRol { idOpcion = 100 , idRol = 1},
                new OpcionRol { idOpcion = 101 , idRol = 1},
                new OpcionRol { idOpcion = 102 , idRol = 1},
                new OpcionRol { idOpcion = 103 , idRol = 1},
                new OpcionRol { idOpcion = 104 , idRol = 1},
                new OpcionRol { idOpcion = 105 , idRol = 1},
                new OpcionRol { idOpcion = 106 , idRol = 1},
                new OpcionRol { idOpcion = 200 , idRol = 1},
                new OpcionRol { idOpcion = 201 , idRol = 1},
                new OpcionRol { idOpcion = 202 , idRol = 1},
                new OpcionRol { idOpcion = 203 , idRol = 1},
                new OpcionRol { idOpcion = 204 , idRol = 1},
                new OpcionRol { idOpcion = 205 , idRol = 1},
                new OpcionRol { idOpcion = 206 , idRol = 1},
                new OpcionRol { idOpcion = 207 , idRol = 1},
                new OpcionRol { idOpcion = 300 , idRol = 1},
                new OpcionRol { idOpcion = 301 , idRol = 1},
                new OpcionRol { idOpcion = 302 , idRol = 1},
                new OpcionRol { idOpcion = 303 , idRol = 1},
                new OpcionRol { idOpcion = 0 , idRol = 2},
                new OpcionRol { idOpcion = 100 , idRol = 2},
                new OpcionRol { idOpcion = 102 , idRol = 2},
                new OpcionRol { idOpcion = 103 , idRol = 2},
                new OpcionRol { idOpcion = 200 , idRol = 2},
                new OpcionRol { idOpcion = 203 , idRol = 2},
                new OpcionRol { idOpcion = 204 , idRol = 2},
                new OpcionRol { idOpcion = 205 , idRol = 2},
                new OpcionRol { idOpcion = 206 , idRol = 2},
                new OpcionRol { idOpcion = 303 , idRol = 2},
                new OpcionRol { idOpcion = 200 , idRol = 3},
                new OpcionRol { idOpcion = 203 , idRol = 3},
                new OpcionRol { idOpcion = 204 , idRol = 3},
                new OpcionRol { idOpcion = 303 , idRol = 3},
                new OpcionRol { idOpcion = 0 , idRol = 4},
                new OpcionRol { idOpcion = 100 , idRol = 4},
                new OpcionRol { idOpcion = 101 , idRol = 4},
                new OpcionRol { idOpcion = 200 , idRol = 5},
                new OpcionRol { idOpcion = 205 , idRol = 5},
                new OpcionRol { idOpcion = 206 , idRol = 5}
            };
            dataOpcionesRol.ForEach(s => context.opcionRol.Add(s));
            context.SaveChanges();

            //-------------------------------------------------------------------------------------

            var dataConfiguraciones = new List<Configuraciones>
            {
                new Configuraciones 
                { 
                    id = 1, 
                    recargaEmergencia = 5500, 
                    intervaloCopiaSeguridad = 30,
                    intervaloMantenimiento = 30,
                    intervaloNotificacion = 30,
                    intervaloSesi�n = 5
                }
            };
            dataConfiguraciones.ForEach(s => context.configuraciones.Add(s));
            context.SaveChanges();

        }
    }
}
