﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Daval.Models
{
    public class Municipio
    {
        [Key]
        public int idMunicipio { get; set; }
        [StringLength(20), Required(ErrorMessage = "Se requiere el nombre del municipio")]
        public string nombre { get; set; }

        public virtual ICollection<Cliente> clientes { get; set; }
    }
}
