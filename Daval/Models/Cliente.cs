﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Daval.Models
{
    public class Cliente
    {

        [Key, RegularExpression("^\\d+$", ErrorMessage = "La cédula digitada no es válida")]
        public string cedula { get; set; }
        [StringLength(30), Required(ErrorMessage="Se requiere el apellido del suscriptor")]
        public string apellido { get; set; }
        [StringLength(30), Required(ErrorMessage = "Se requiere el apellido del suscriptor")]
        public string nombre { get; set; }
        [StringLength(30), Required(ErrorMessage = "Se requiere la dirección del suscriptor")]
        public string direccion { get; set; }
        [StringLength(10, MinimumLength = 7, ErrorMessage = "El teléfono digitado no es válido"), Required(ErrorMessage = "Se requiere el teléfono del suscriptor"), 
        RegularExpression("^\\d+$", ErrorMessage="El teléfono digitado no es válido")]
        public string telefono { get; set; }
        public int idMunicipio { get; set; }
        public int idEstrato { get; set; }
        
        public virtual Estrato estrato { get; set; }
        public virtual Municipio municipio { get; set; }        
    }
}