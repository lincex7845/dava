﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Daval.Models
{
    public class OpcionRol
    {
        [Key]
        [Column("idRol", Order = 0)]
        public int idRol { get; set; }
        [Key]
        [Column("idOpcion", Order = 1)]
        public int idOpcion { get; set; }

        public virtual Rol rol { get; set; }
        public virtual Opcion opcion { get; set; }
    }
}