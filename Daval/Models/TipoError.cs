﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Daval.Models
{
   public class TipoError
    {
        [Key]
        public int idTipoError { get; set; }
        [StringLength(30), Required(ErrorMessage = "Se requiere la descripción del error")]
        public string descripcion { get; set; }
    }
}
