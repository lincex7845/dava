﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Daval.Models
{
   public class Estrato
    {
       [Key]
       public int idEstrato { get; set; }
       [StringLength(20), Required(ErrorMessage = "Se requiere el estrato")]
       public string estratoNom { get; set; }
       [Required(ErrorMessage = "Se requiere un precio para el estrato")]
       public int valor { get; set; }
    }
}
