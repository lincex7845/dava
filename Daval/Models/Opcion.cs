﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Daval.Models
{
    public class Opcion
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idOpcion { get; set; }
        [StringLength(50), Required(ErrorMessage="Se requiere la descripción de la opción")]
        public string nombreOpcion { get; set; }
    }
}