﻿using System;
using System.ComponentModel.DataAnnotations;


namespace Daval.Models
{
    public class IniciarSesionModelo
    {
        [Required(ErrorMessage = "El usuario es requerido para iniciar sesión")]
        [Display(Name = "Usuario")]
        public string usuario { get; set; }

        [Required(ErrorMessage = "La contraseña es requerida para iniciar sesión")]
        [DataType(DataType.Password)]
        [Display(Name = "pass")]
        public string pass { get; set; }
    }

    public class RegistrarModelo
    {
        [Required(ErrorMessage="El usuario es requerido para registrarse")]
        [Display(Name = "Usuario")]
        public string usuario { get; set; }

        [Required(ErrorMessage = "La contraseña es requerida para registrarse")]
        [StringLength(10, ErrorMessage = "La {0} debe tener al menos {2} caracteres.", MinimumLength = 5)]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string pass { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirme la contraseña")]
        [Compare("pass", ErrorMessage = "Las contraseñas no concuerdan.")]
        public string confirmarPass { get; set; }
    }

    public class PerfilModelo : Usuario
    {
        //[StringLength(30), Required(ErrorMessage = "Se requiere un apellido para el usuario")]
        //public string apellido { get; set; }

        //[StringLength(30), Required(ErrorMessage = "Se requiere un nombre para el usuario")]
        //public string nombre { get; set;}

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña Actual")]
        public string passActual { get; set; }

        [Required]
        [StringLength(10, ErrorMessage = "La {0} debe tener al menos {2} caracteres.", MinimumLength = 5)]
        [DataType(DataType.Password)]
        [Display(Name = "Nueva Contraseña")]
        public string passNueva { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirme la contraseña")]
        [Compare("passNueva", ErrorMessage = "Las contraseñas no concuerdan.")]
        public string confirmarPass { get; set; }
    }
}