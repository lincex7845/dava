﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Daval.Models
{
    public class Usuario
    {
        [Key]
        [Column("idUsuario", Order = 0)]
        public int idUsuario { get; set; }
        [StringLength(30), Required(ErrorMessage = "Se requiere un apellido para el usuario")]
        public string apellido { get; set; }
        [StringLength(30), Required(ErrorMessage = "Se requiere un nombre para el usuario")]
        public string nombre { get; set;}
        [StringLength(10), Required(ErrorMessage = "Se requiere el usuario a asignar")]
        public string usuario { get; set; }
        [StringLength(10), Required(ErrorMessage = "Se requiere una contraseña para el usuario")]
        public string contraseña { get; set; }
        public int idRol { get; set; }
        public int idEstado { get; set; }

        public virtual Rol rol { get; set; }
        public virtual Estado estado { get; set; }
    }
}