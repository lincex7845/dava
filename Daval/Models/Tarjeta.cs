﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Daval.Models
{
   public class Tarjeta
    {
       [Key]
       public int idTarjeta { get; set; }
       [Required(ErrorMessage = "Se requiere el serial de la tarjeta")]
       public string serial { get; set; }
       [Required(ErrorMessage = "Se requiere el lote de la tarjeta")]
       public string lote { get; set; }
       [StringLength(10), Required(ErrorMessage = "Se requiere la contraseña de la tarjeta")]
       public string contraseña { get; set; }
       [Required(ErrorMessage = "Se requiere la fecha de entrada de la tarjeta")]
       public DateTime fecha_entrada { get; set; }
       public DateTime? fecha_salida { get; set; }
       public int idEstado { get; set; }

       public virtual Estado estado { get; set; }
       //public virtual ClienteTarjeta TarjetaCliente { get; set; }
       public virtual ICollection<Recarga> recargas { get; set; }
    }
}
