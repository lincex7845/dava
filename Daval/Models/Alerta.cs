﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Daval.Models
{
    public class Alerta
    {
        [Key]
        public int idAlerta { get; set; }
        public int idMedidor { get; set; }
        public int idTipoAlerta { get; set; }
        [Required(ErrorMessage = "Se requiere la fecha del evento")]
        public DateTime fecha { get; set; }

        public virtual Medidor medidor { get; set; }
        public virtual TipoAlerta tiposAlerta { get; set; }
    }
}