﻿using System;

namespace Daval.Models
{
    public class RegistrarTarjetas
    {
        public string serial            { get; set; }
	    public string lote              { get; set; }
        public string contrasena        { get; set; }
	    public DateTime fecha_entrada	{ get; set; }
        public DateTime? fecha_salida   { get; set; }
        public int idEstado             { get; set; }
    }
}