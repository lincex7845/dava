﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Daval.Models
{
   public class Recarga
    {
       [Key]
       [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
       public Guid idRecarga { get; set; }
       public float mcc { get; set; }                  // METROS CUBICOS COMPRADOS 
       public float valor { get; set; }                // Valor Compra
       public DateTime fecRecarga { get; set; }
       public String idTarjeta { get; set; }
       public int idUsuario { get; set; }
       public int idPos { get; set; }
       public int idEstado { get; set; }

       public virtual Tarjeta tarjeta { get; set; }
       public virtual Usuario usuario { get; set; }
       public virtual POS pos { get; set; }
       public virtual Estado estado { get; set; }

    }
}
