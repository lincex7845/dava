﻿using System.ComponentModel.DataAnnotations;

namespace Daval.Models
{
    public class Configuraciones
    {
        [Key]
        public int id { get; set; }

        [Required, Display(Name = "Recarga de Emergencia")]
        public int recargaEmergencia { get; set; }

        [Required, Range(0, 60), Display(Name = "Intervalo para Copia de Seguridad")]
        public int intervaloCopiaSeguridad { get; set; }

        [Required, Range(0, 60), Display(Name = "Intervalo para Notificaciones")]
        public int intervaloNotificacion { get; set; }

        [Required, Range(0, 60), Display(Name = "Intervalo para Mantenimiento")]
        public int intervaloMantenimiento { get; set; }

        [Required, Range(0, 60), Display(Name = "Tiempo de Expiración de Sesión")]
        public int intervaloSesión { get; set; }
    }
}