﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Daval.Models
{
    public class RegistrarMedidores 
    {
        public string       serial                  {get; set;}
        public string       lote                    {get; set;} 
        public DateTime     fecha_entrada           {get; set;}
        public DateTime?    fecha_salida            {get; set;} 
        public float        mca			            {get; set;}
        public float        mcc			            {get; set;}
        [Range(0, 100, ErrorMessage = "El nivel de bateria debe estar entre 1 % y 100 %")]
        public int          nivelBateria	        {get; set;}
        public int          idEstado                { get; set; }
    }
}