﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Daval.Models
{
    public class HistorialSesion
    {
        [Key]
        public int idHistSesion { get; set; }
        public int idUsuario { get; set; }
        public int idEstado { get; set; }
        public DateTime fechaInicio { get; set; }
        public DateTime fechaFin { get; set; }
        public string ip { get; set; }
        public string token { get; set; }

        public virtual Usuario usuario { get; set; }
        public virtual Estado estado { get; set; }

    }
}