﻿using System.Collections.Generic;

namespace Daval.Models
{
    public class Disponibles
    {
        public List<ClienteDisponible> ClientesDisponibles { get; set; }
        public List<MedidorDisponible> MedidoresDisponibles { get; set; }
        public List<TarjetaDisponible> TarjetasDisponibles { get; set; }
    }

    public class ClienteDisponible
    {
        public string Cedula { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
    }

    public class MedidorDisponible
    {
        public string Serial { get; set; }
    }

    public class TarjetaDisponible
    {
        public string Serial { get; set; }
    }
}