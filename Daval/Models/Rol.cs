﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Daval.Models
{
    public class Rol
    {
        [Key]
        public int idRol {get; set;}
        [StringLength(30), Required(ErrorMessage = "Se requiere un nombre para el rol")]
        public string rolName { get; set;}

        public virtual ICollection<Usuario> usuarios { get; set; }
    }
}