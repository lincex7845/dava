﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Daval.Models
{
    public class Estado
    {
        [Key]
        public int idEstado { get; set; }
        [StringLength(20), Required(ErrorMessage = "Se requiere un nombre para el estado")]
        public string descripcion { get; set; }

        public virtual ICollection<Usuario> usuarios { get; set; }
        public virtual ICollection<Medidor> medidores { get; set; }
        public virtual ICollection<Tarjeta> tarjetas { get; set; }
    }
}
