﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Daval.Models
{
    public class POA
    {
        [Key]
        public int idPOA { get; set; }
        [StringLength(20), Required(ErrorMessage = "Se requiere un nombre")]
        public string nombre { get; set; }
        [StringLength(50), Required(ErrorMessage = "Se requiere la dirección")]
        public string dirección { get; set; }
        public DateTime fecha_reg { get; set; }
        public int idEstado { get; set; }
        public string token { get; set; }

        public virtual Estado estados { get; set; }
    }
}