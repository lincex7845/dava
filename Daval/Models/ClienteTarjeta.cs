﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Daval.Models
{
    public class ClienteTarjeta
    {
        [Key]
        [Column("Cliente", Order = 0)]
        public string cedula { get; set; }
        [Key]
        [Column("Tarjeta", Order = 1)]
        public int idTarjeta { get; set; }

        //[ForeignKey("cedula")]
        public virtual Cliente cliente { get; set; }
        //[ForeignKey("idTarjeta")]
        public virtual Tarjeta tarjeta { get; set; }
    }
}