﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Daval.Models
{
    public class Error
    {
        [Key]
        public int idError { get; set; }
        public int idPOS { get; set; }
        public int idTipoError { get; set; }
        [Required(ErrorMessage="Se requiere la fecha del evento")]
        public DateTime fecha { get; set; }

        public virtual POS pos { get; set; }
        public virtual TipoError tiposAlerta { get; set; }
    }
}