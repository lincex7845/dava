﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Daval.Models
{
    public class DavalDBContext: DbContext
    {

        public DavalDBContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<Estado> estados { get; set; }
        public DbSet<Estrato> estratos { get; set; }
        public DbSet<Rol> roles { get; set; }
        public DbSet<Opcion> opciones { get; set; }
        public DbSet<OpcionRol> opcionRol { get; set; }
        public DbSet<Municipio> municipios { get; set; }
        public DbSet<TipoAlerta> tipoAlertas { get; set; }
        public DbSet<TipoError> tipoError { get; set; }
        public DbSet<Alerta> alertas { get; set; }
        public DbSet<Error> errores { get; set; }
        public DbSet<POS> pos { get; set; }
        public DbSet<POA> poa { get; set; }
        public DbSet<Usuario> usuarios { get; set; }
        public DbSet<Medidor> medidores { get; set; }
        public DbSet<Tarjeta> tarjetas { get; set; }
        public DbSet<Cliente> clientes { get; set; }
        public DbSet<ClienteMedidor> clienteMedidor { get; set; }
        public DbSet<ClienteTarjeta> clienteTarjeta { get; set; }        
        public DbSet<HistorialSesion> historialSesion { get; set; }
        public DbSet<Recarga> recargas { get; set; }
        public DbSet<Configuraciones> configuraciones { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}