﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Daval.Models
{
    public class Medidor
    {
        [Key]
        public int idMedidor { get; set; }
        [Required(ErrorMessage = "Se requiere el serial del medidor")]
        public string serial { get; set; }
        [Required(ErrorMessage = "Se requiere el lote del medidor")]
        public string lote { get; set; }
        [Required(ErrorMessage = "Se requiere la fecha de entrada del medidor")]
        public DateTime fecha_entrada { get; set; }
        public DateTime? fecha_salida { get; set; }
        public float mca { get; set; }                      // metros cubicos acumulados
        public float mcc { get; set; }                      // metros cubicos cosumidos
        [Range(0, 1000)]
        public float nivelBateria { get; set; }             // En voltios
        public int idEstado { get; set; }
        
        public virtual Estado estados { get; set; }
        //public virtual ClienteMedidor MedidorCliente { get; set; }
    }
        
}