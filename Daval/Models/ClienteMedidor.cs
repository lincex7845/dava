﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Daval.Models
{
    public class ClienteMedidor
    {
        [Key]
        [Column("Cliente", Order = 0)]
        public string cedula { get; set; }
        [Key]
        [Column("Medidor", Order = 1)]
        public int idMedidor { get; set; }

        //[ForeignKey("cedula")]
        public virtual Cliente cliente { get; set; }
        //[ForeignKey("idMedidor")]
        public virtual Medidor medidor { get; set; }
        
    }
}