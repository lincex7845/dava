﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Daval.Models
{
    public class TipoAlerta
    {
        [Key]
        public int idTipoAlerta { get; set; }
        [StringLength(30), Required(ErrorMessage = "Se requiere una descripción de la alerta")]
        public string descripcion { get; set; }
    }
}