﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Daval.Models;

namespace Daval.Controllers
{
    public class ErrorController : Controller
    {
        private DavalDBContext db = new DavalDBContext();
        private Validacion v = Validacion.Instance;
        private const string opcion = "Historial de Errores";
        //
        // GET: /Error/

        public ActionResult Index()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "nlightblue";
                    ViewBag.Estadisticas = "norange current open";
                    var errores = db.errores.Include(e => e.pos).Include(e => e.tiposAlerta);
                    return View(errores.ToList());
                }
                else
                    return RedirectToAction("Restringido", "Home");
            }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // GET: /Error/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    if (Session["user"] != null && Session["nom"] != null)
        //    {
        //        TempData["nom"] = Session["nom"].ToString();
        //        TempData["user"] = Session["user"].ToString();
        //        ViewBag.Panel = "nlightblue";
        //        ViewBag.Seguridad = "nlightblue";
        //        ViewBag.Servicios = "nlightblue";
        //        ViewBag.Estadisticas = "norange current open";
        //        Error error = db.errores.Find(id);
        //        if (error == null)
        //        {
        //            return HttpNotFound();
        //        }
        //        return View(error);
        //    }
        //    else 
        //        return RedirectToAction("IniciarSesion", "Sesion");
        //}

        ////
        //// GET: /Error/Create

        //public ActionResult Create()
        //{
        //    if (Session["user"] != null && Session["nom"] != null)
        //    {
        //        TempData["nom"] = Session["nom"].ToString();
        //        TempData["user"] = Session["user"].ToString();
        //        ViewBag.Panel = "nlightblue";
        //        ViewBag.Seguridad = "nlightblue";
        //        ViewBag.Servicios = "nlightblue";
        //        ViewBag.Estadisticas = "norange current open";
        //        ViewBag.idPOS = new SelectList(db.pos, "idPOS", "nombre");
        //        ViewBag.idTipoError = new SelectList(db.tipoError, "idTipoError", "descripcion");
        //        return View();
        //        }
        //    else 
        //        return RedirectToAction("IniciarSesion", "Sesion");
        //}

        ////
        //// POST: /Error/Create

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create(Error error)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.errores.Add(error);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.idPOS = new SelectList(db.pos, "idPOS", "nombre", error.idPOS);
        //    ViewBag.idTipoError = new SelectList(db.tipoError, "idTipoError", "descripcion", error.idTipoError);
        //    return View(error);
        //}

        ////
        //// GET: /Error/Edit/5

        //public ActionResult Edit(int id = 0)
        //{
        //    if (Session["user"] != null && Session["nom"] != null)
        //    {
        //        TempData["nom"] = Session["nom"].ToString();
        //        TempData["user"] = Session["user"].ToString();
        //        ViewBag.Panel = "nlightblue";
        //        ViewBag.Seguridad = "nlightblue";
        //        ViewBag.Servicios = "nlightblue";
        //        ViewBag.Estadisticas = "norange current open";
        //        Error error = db.errores.Find(id);
        //        if (error == null)
        //        {
        //            return HttpNotFound();
        //        }
        //        ViewBag.idPOS = new SelectList(db.pos, "idPOS", "nombre", error.idPOS);
        //        ViewBag.idTipoError = new SelectList(db.tipoError, "idTipoError", "descripcion", error.idTipoError);
        //        return View(error);
        //    }
        //    else 
        //        return RedirectToAction("IniciarSesion", "Sesion");
        //}

        ////
        //// POST: /Error/Edit/5

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit(Error error)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(error).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.idPOS = new SelectList(db.pos, "idPOS", "nombre", error.idPOS);
        //    ViewBag.idTipoError = new SelectList(db.tipoError, "idTipoError", "descripcion", error.idTipoError);
        //    return View(error);
        //}

        ////
        //// GET: /Error/Delete/5

        //public ActionResult Delete(int id = 0)
        //{
        //    if (Session["user"] != null && Session["nom"] != null)
        //    {
        //        TempData["nom"] = Session["nom"].ToString();
        //        TempData["user"] = Session["user"].ToString();
        //        ViewBag.Panel = "nlightblue";
        //        ViewBag.Seguridad = "nlightblue";
        //        ViewBag.Servicios = "nlightblue";
        //        ViewBag.Estadisticas = "norange current open";
        //        Error error = db.errores.Find(id);
        //        if (error == null)
        //        {
        //            return HttpNotFound();
        //        }
        //        return View(error);
        //    }
        //    else 
        //        return RedirectToAction("IniciarSesion", "Sesion");
        //}

        ////
        //// POST: /Error/Delete/5

        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Error error = db.errores.Find(id);
        //    db.errores.Remove(error);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}