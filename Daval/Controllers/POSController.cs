﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Daval.Models;
using Daval.Controllers.DSTableAdapters;

namespace Daval.Controllers
{
    public class POSController : Controller
    {
        private DavalDBContext db = new DavalDBContext();
        private Validacion v = Validacion.Instance;
        private const string opcion = "Gestión de Puntos de Venta";

        //
        // GET: /POS/

        public ActionResult Index()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    var pos = db.pos.Include(p => p.estados);
                    return View(pos.ToList());
                }
                else
                    return RedirectToAction("Restringido", "Home");
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        ////
        //// GET: /POS/Details/5
        //public ActionResult Details(int id = 0)
        //{
        //    if (Session["user"] != null && Session["nom"] != null)
        //    {
        //        TempData["nom"] = Session["nom"].ToString();
        //        TempData["user"] = Session["user"].ToString();
        //        ViewBag.Panel = "nlightblue";
        //        ViewBag.Seguridad = "nlightblue";
        //        ViewBag.Servicios = "norange current open";
        //        ViewBag.Estadisticas = "nlightblue";
        //        POS pos = db.pos.Find(id);
        //        if (pos == null)
        //        {
        //            return HttpNotFound();
        //        }
        //        return View(pos);
        //     }
        //    else 
        //        return RedirectToAction("IniciarSesion", "Sesion");
        //}

        //
        // GET: /POS/Create

        public ActionResult Create()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion");
                    return View();
                }
                else 
                    return RedirectToAction("Restringido", "Home");
             }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /POS/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(POS pos)
        {
            string salida = "";
            pos.fecha_reg = DateTime.UtcNow;
            pos.token = new GenerarTokenTableAdapter().GenToken(salida: ref salida).ToString();
            if (ModelState.IsValid)
            {
                db.pos.Add(pos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion", pos.idEstado);
            return View(pos);
        }

        //
        // GET: /POS/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    POS pos = db.pos.Find(id);
                    if (pos == null)
                    {
                        return HttpNotFound();
                    }
                    ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion", pos.idEstado);
                    return View(pos);
                }
                else 
                    return RedirectToAction("Restringido", "Home");
             }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /POS/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(POS pos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion", pos.idEstado);
            return View(pos);
        }

        //
        // GET: /POS/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    POS pos = db.pos.Find(id);
                    if (pos == null)
                    {
                        return HttpNotFound();
                    }
                    return View(pos);
                }
                else 
                    return RedirectToAction("Restringido", "Home");
             }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /POS/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            POS pos = db.pos.Find(id);
            db.pos.Remove(pos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}