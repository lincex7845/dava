﻿using System.Data;
using System.Linq;
using System.Web.Mvc;
using Daval.Models;

namespace Daval.Controllers
{
    public class RolController : Controller
    {
        private DavalDBContext db = new DavalDBContext();
        private Validacion v = Validacion.Instance;
        private const string opcion = "Gestión de Roles";
        //
        // GET: /Rol/Index

        public ActionResult Index()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "norange current open";
                    ViewBag.Servicios = "nlightblue";
                    ViewBag.Estadisticas = "nlightblue";
                    return View(db.roles.ToList());
                }
                else
                    return RedirectToAction("Restringido", "Home");
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }
        
        //
        // GET: /Rol/Create

        public ActionResult Create()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "norange current open";
                    ViewBag.Servicios = "nlightblue";
                    ViewBag.Estadisticas = "nlightblue";
                    return View();
                 }
                else 
                    return RedirectToAction("Restringido", "Home");
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /Rol/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Rol rol)
        {
            if (ModelState.IsValid)
            {
                db.roles.Add(rol);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(rol);
        }

        //
        // GET: /Rol/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "norange current open";
                    ViewBag.Servicios = "nlightblue";
                    ViewBag.Estadisticas = "nlightblue";
                    Rol rol = db.roles.Find(id);
                    if (rol == null)
                    {
                        return HttpNotFound();
                    }
                    return View(rol);
                 }
                else 
                    return RedirectToAction("Restringido", "Home");
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /Rol/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Rol rol)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rol).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(rol);
        }

        //
        // GET: /Rol/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "norange current open";
                    ViewBag.Servicios = "nlightblue";
                    ViewBag.Estadisticas = "nlightblue";
                    Rol rol = db.roles.Find(id);
                    if (rol == null)
                    {
                        return HttpNotFound();
                    }
                    return View(rol);
                 }
                else 
                    return RedirectToAction("Restringido", "Home");
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /Rol/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Rol rol = db.roles.Find(id);
            db.roles.Remove(rol);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}