﻿using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Daval.Models;

namespace Daval.Controllers
{
    public class ClienteController : Controller
    {
        private DavalDBContext db = new DavalDBContext();
        private Validacion v = Validacion.Instance;
        //private Disponibles disponibles = new  Disponibles();
        //private List<ClienteDisponible> clienteDisponibles = new List<ClienteDisponible>();
        //private List<MedidorDisponible> medidorDisponibles = new List<MedidorDisponible>();
        //private List<TarjetaDisponible> tarjetaDisponibles = new List<TarjetaDisponible>();
        private const string opcion = "Gestión de Clientes";
        //
        // GET: /Cliente/

        public ActionResult Index()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    var clientes = db.clientes.Include(c => c.estrato).Include(c => c.municipio);
                    return View(clientes.ToList());
                }
                else
                    return RedirectToAction("Restringido", "Home");

            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // GET: /Cliente/Details/5

        public ActionResult Details(string id)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    Cliente cliente = db.clientes.Find(id);
                    if (cliente == null)
                    {
                        return HttpNotFound();
                    }
                    return View(cliente);
                }
                else 
                    return RedirectToAction("Restringido", "Home");
            }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // GET: /Cliente/Create

        public ActionResult Create()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    ViewBag.idEstrato = new SelectList(db.estratos, "idEstrato", "estratoNom");
                    ViewBag.idMunicipio = new SelectList(db.municipios, "idMunicipio", "nombre");
                    return View();
                }
                else 
                    return RedirectToAction("Restringido", "Home");
            }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /Cliente/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Cliente clienteNuevo)
        {
            if (ModelState.IsValid)
            {
                db.clientes.Add(clienteNuevo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idEstrato = new SelectList(db.estratos, "idEstrato", "estratoNom", clienteNuevo.idEstrato);
            ViewBag.idMunicipio = new SelectList(db.municipios, "idMunicipio", "nombre", clienteNuevo.idMunicipio);
            return View(clienteNuevo);
        }

        //
        // GET: /Cliente/Edit/5

        public ActionResult Edit(string id)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    Cliente cliente = db.clientes.Find(id);
                    if (cliente == null)
                    {
                        return HttpNotFound();
                    }
                    ViewBag.idEstrato = new SelectList(db.estratos, "idEstrato", "estratoNom", cliente.idEstrato);
                    ViewBag.idMunicipio = new SelectList(db.municipios, "idMunicipio", "nombre", cliente.idMunicipio);
                    return View(cliente);
                }
                else 
                    return RedirectToAction("Restringido", "Home");
            }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /Cliente/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Cliente cliente)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cliente).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idEstrato = new SelectList(db.estratos, "idEstrato", "estratoNom", cliente.idEstrato);
            ViewBag.idMunicipio = new SelectList(db.municipios, "idMunicipio", "nombre", cliente.idMunicipio);
            return View(cliente);
        }

        
        //GET: /Cliente/Medidor/5
        //public ActionResult Medidor(string id)
        //{
        //    if (Session["user"] != null && Session["nom"] != null)
        //    {
        //        TempData["nom"] = Session["nom"].ToString();
        //        TempData["user"] = Session["user"].ToString();
        //        ViewBag.Panel = "nlightblue";
        //        ViewBag.Seguridad = "nlightblue";
        //        ViewBag.Servicios = "norange current open";
        //        ViewBag.Estadisticas = "nlightblue";
        //         Buscar Tarjetas disponibles
        //        DS.TarjetasDisponiblesDataTable TDDT = new DS.TarjetasDisponiblesDataTable();
        //        TDDT.Clear();
        //        TarjetasDisponiblesTableAdapter tarjetas = new TarjetasDisponiblesTableAdapter();
        //        tarjetas.Fill(TDDT);
        //         Guardarlas en una lista
        //        TarjetasDispCliente TDC = new TarjetasDispCliente();
        //        Cliente cliente = db.clientes.Find(id);
        //        string[] listaTarjetas = new string[TDDT.Count];
        //        int i = 0;
        //        foreach(DS.TarjetasDisponiblesRow r in TDDT.Rows)
        //        {
        //            listaTarjetas[i] = r.Serial.ToString();
        //        }

        //        return View(TDDT);
        //    }
        //    else
        //        return RedirectToAction("IniciarSesion", "Sesion");
        //}

        //
        // GET: /Cliente/Delete/5

        public ActionResult Delete(string id)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    Cliente cliente = db.clientes.Find(id);
                    if (cliente == null)
                    {
                        return HttpNotFound();
                    }
                    return View(cliente);
                }
                else 
                    return RedirectToAction("Restringido", "Home");
            }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /Cliente/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {

                Cliente cliente = db.clientes.Find(id);
                db.clientes.Remove(cliente);
                db.SaveChanges();
                return RedirectToAction("Index");
        }

        // GET: /Cliente/Tarjeta
        /*public ActionResult Tarjeta()
        {
            clienteDisponibles.Clear();
            tarjetaDisponibles.Clear();
            // *** Clientes Disponibles ***
            DS.ClientesDisponiblesTarjetasDataTable CDDT = new DS.ClientesDisponiblesTarjetasDataTable();
            ClientesDisponiblesTarjetasTableAdapter CTTA = new ClientesDisponiblesTarjetasTableAdapter();
            CDDT.Clear();
            CTTA.Fill(CDDT);
            foreach (DS.ClientesDisponiblesTarjetasRow r in CDDT.Rows)
                clienteDisponibles.Add(
                    new ClienteDisponible
                    {
                        Cedula = r.cedula,
                        Nombre = r.nombre,
                        Apellido = r.apellido
                    }
                );
            // *** Tarjetas Disponibles ***
            DS.TarjetasDisponiblesDataTable TDDT = new DS.TarjetasDisponiblesDataTable();
            TarjetasDisponiblesTableAdapter TDTA = new TarjetasDisponiblesTableAdapter();
            TDDT.Clear();
            TDTA.Fill(TDDT);
            foreach (DS.TarjetasDisponiblesRow r in TDDT.Rows)
                tarjetaDisponibles.Add(
                    new TarjetaDisponible { Serial = r.Serial }
                );
            disponibles.ClientesDisponibles = this.clienteDisponibles;
            disponibles.TarjetasDisponibles = this.tarjetaDisponibles;            
            return View(disponibles);
        }*/

        // GET: /Cliente/Medidor
        /*public ActionResult Medidor()
        {
            disponibles.ClientesDisponibles.Clear();
            disponibles.MedidoresDisponibles.Clear();
            disponibles.TarjetasDisponibles.Clear();
            // *** Clientes Disponibles ***
            DS.ClientesDisponiblesTarjetasDataTable CDDT = new DS.ClientesDisponiblesTarjetasDataTable();
            ClientesDisponiblesTarjetasTableAdapter CTTA = new ClientesDisponiblesTarjetasTableAdapter();
            CDDT.Clear();
            CTTA.Fill(CDDT);
            foreach (DS.ClientesDisponiblesTarjetasRow r in CDDT.Rows)
                disponibles.ClientesDisponibles.Add(
                    new ClienteDisponible
                    {
                        Cedula = r.cedula,
                        Nombre = r.nombre,
                        Apellido = r.apellido
                    }
                );
            // *** Medidores Disponibles ***
            DS.MedidoresDisponiblesDataTable MDDT = new DS.MedidoresDisponiblesDataTable();
            MedidoresDisponiblesTableAdapter MDTA = new MedidoresDisponiblesTableAdapter();
            MDDT.Clear();
            MDTA.Fill(MDDT);
            foreach (DS.TarjetasDisponiblesRow r in MDDT.Rows)
                disponibles.MedidoresDisponibles.Add(
                    new MedidorDisponible { Serial = r.Serial }
                );
            return View(disponibles);
        }*/

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}