﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Daval.Models;

namespace Daval.Controllers
{
    public class ClienteTarjetaController : Controller
    {
        private DavalDBContext db = new DavalDBContext();
        private Validacion v = Validacion.Instance;
        private const string opcion = "Relación de Clientes y Tarjetas";
        //
        // GET: /ClienteTarjeta/

        public ActionResult Index()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "nlightblue";
                    ViewBag.Estadisticas = "norange current open";
                    var clientetarjeta = db.clienteTarjeta.Include(c => c.cliente).Include(c => c.tarjeta);
                    return View(clientetarjeta.ToList());
                }
                else
                    return RedirectToAction("Restringido", "Home");

            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // GET: /ClienteTarjeta/Details/5

        /*public ActionResult Details(string id = null)
        {
            ClienteTarjeta clientetarjeta = db.clienteTarjeta.Find(id);
            if (clientetarjeta == null)
            {
                return HttpNotFound();
            }
            return View(clientetarjeta);
        }*/

        //
        // GET: /ClienteTarjeta/Create

        /*public ActionResult Create()
        {
            ViewBag.cedula = new SelectList(db.clientes, "cedula", "apellido");
            ViewBag.idTarjeta = new SelectList(db.tarjetas, "idTarjeta", "serial");
            return View();
        }*/

        //
        // POST: /ClienteTarjeta/Create

        /*[HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ClienteTarjeta clientetarjeta)
        {
            if (ModelState.IsValid)
            {
                db.clienteTarjeta.Add(clientetarjeta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.cedula = new SelectList(db.clientes, "cedula", "apellido", clientetarjeta.cedula);
            ViewBag.idTarjeta = new SelectList(db.tarjetas, "idTarjeta", "serial", clientetarjeta.idTarjeta);
            return View(clientetarjeta);
        }*/

        //
        // GET: /ClienteTarjeta/Edit/5

        /*public ActionResult Edit(string id = null)
        {
            ClienteTarjeta clientetarjeta = db.clienteTarjeta.Find(id);
            if (clientetarjeta == null)
            {
                return HttpNotFound();
            }
            ViewBag.cedula = new SelectList(db.clientes, "cedula", "apellido", clientetarjeta.cedula);
            ViewBag.idTarjeta = new SelectList(db.tarjetas, "idTarjeta", "serial", clientetarjeta.idTarjeta);
            return View(clientetarjeta);
        }*/

        //
        // POST: /ClienteTarjeta/Edit/5

        /*[HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ClienteTarjeta clientetarjeta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(clientetarjeta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.cedula = new SelectList(db.clientes, "cedula", "apellido", clientetarjeta.cedula);
            ViewBag.idTarjeta = new SelectList(db.tarjetas, "idTarjeta", "serial", clientetarjeta.idTarjeta);
            return View(clientetarjeta);
        }*/

        //
        // GET: /ClienteTarjeta/Delete/5

        /*public ActionResult Delete(string id = null)
        {
            ClienteTarjeta clientetarjeta = db.clienteTarjeta.Find(id);
            if (clientetarjeta == null)
            {
                return HttpNotFound();
            }
            return View(clientetarjeta);
        }*/

        //
        // POST: /ClienteTarjeta/Delete/5

        /*[HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ClienteTarjeta clientetarjeta = db.clienteTarjeta.Find(id);
            db.clienteTarjeta.Remove(clientetarjeta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }*/

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}