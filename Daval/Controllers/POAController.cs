﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Daval.Models;
using Daval.Controllers.DSTableAdapters;

namespace Daval.Controllers
{
    public class POAController : Controller
    {
        private DavalDBContext db = new DavalDBContext();
        private Validacion v = Validacion.Instance;
        private const string opcion = "Gestión de Puntos de Administración";
        //
        // GET: /POA/

        public ActionResult Index()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    var poa = db.poa.Include(p => p.estados);
                    return View(poa.ToList());
                }
                else
                    return RedirectToAction("Restringido", "Home");
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        ////
        //// GET: /POA/Details/5
        //public ActionResult Details(int id = 0)
        //{
        //    if (Session["user"] != null && Session["nom"] != null)
        //    {
        //        TempData["nom"] = Session["nom"].ToString();
        //        TempData["user"] = Session["user"].ToString();
        //        ViewBag.Panel = "nlightblue";
        //        ViewBag.Seguridad = "nlightblue";
        //        ViewBag.Servicios = "norange current open";
        //        ViewBag.Estadisticas = "nlightblue";
        //        POA poa = db.poa.Find(id);
        //        if (poa == null)
        //        {
        //            return HttpNotFound();
        //        }
        //        return View(poa);
        //     }
        //    else 
        //        return RedirectToAction("IniciarSesion", "Sesion");
        //}

        //
        // GET: /POA/Create

        public ActionResult Create()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion");
                    return View();
                }
                else 
                    return RedirectToAction("Restringido", "Home");
             }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /POA/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(POA poa)
        {
            string salida = "";
            poa.fecha_reg = DateTime.UtcNow;
            poa.token = new GenerarTokenTableAdapter().GenToken(salida: ref salida).ToString();
            if (ModelState.IsValid)
            {
                db.poa.Add(poa);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion", poa.idEstado);
            return View(poa);
        }

        //
        // GET: /POA/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    POA poa = db.poa.Find(id);
                    if (poa == null)
                    {
                        return HttpNotFound();
                    }
                    ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion", poa.idEstado);
                    return View(poa);
                }
                else 
                    return RedirectToAction("Restringido", "Home");
             }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /POA/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(POA poa)
        {
            if (ModelState.IsValid)
            {
                db.Entry(poa).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion", poa.idEstado);
            return View(poa);
        }

        //
        // GET: /POA/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    POA poa = db.poa.Find(id);
                    if (poa == null)
                    {
                        return HttpNotFound();
                    }
                    return View(poa);
                }
                else 
                    return RedirectToAction("Restringido", "Home");
             }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /POA/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            POA poa = db.poa.Find(id);
            db.poa.Remove(poa);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}