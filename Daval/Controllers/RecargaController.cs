﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Daval.Models;
using Daval.Controllers.DSTableAdapters;
using System.Collections.Generic;

namespace Daval.Controllers
{
    public class RecargaController : Controller
    {
        private DavalDBContext db = new DavalDBContext();
        private Validacion v = Validacion.Instance;
        private const string opcion = "Historial de Recargas";
        private const string opcion2 = "Relación de Recargas y Puntos de Venta";
        //
        // GET: /Recarga/

        public ActionResult Index()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "nlightblue";
                    ViewBag.Estadisticas = "norange current open";
                    var recargas = db.recargas.Include(r => r.tarjeta).Include(r => r.usuario).Include(r => r.pos).Include(r => r.estado);
                    return View(recargas.ToList());
                }
                else
                    return RedirectToAction("Restringido", "Home");
            }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // GET: /Recarga/Details/5

        public ActionResult Details(Guid id)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "nlightblue";
                    ViewBag.Estadisticas = "norange current open";
                    Recarga recarga = db.recargas.Find(id);
                    if (recarga == null)
                    {
                        return HttpNotFound();
                    }
                    return View(recarga);
                }
                else 
                    return RedirectToAction("Restringido", "Home");
            }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        // GET: /Recarga/POS
        public ActionResult POS()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion2))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "nlightblue";
                    ViewBag.Estadisticas = "norange current open";
                    var pos = db.pos.Include(p => p.estados);
                    return View(pos.ToList());
                }
                else
                    return RedirectToAction("Restringido", "Home");
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        // POST: /Recarga/POS
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult POS(int idPOS)
        {
            DS.RecargaByPOSDataTable RDT = new DS.RecargaByPOSDataTable();
            RecargaByPOSTableAdapter RTA = new RecargaByPOSTableAdapter();
            List<Recarga> recargasPOS = new List<Recarga>();
            RDT.Clear();
            RTA.Fill(RDT, idPOS);
            foreach (DS.RecargaByPOSRow r in RDT.Rows)
                recargasPOS.Add(
                    new Recarga
                    {
                        idRecarga = r.idRecarga,
                        mcc = r.mcc,
                        valor = r.valor,
                        fecRecarga = r.fecRecarga
                    }
                );
            return Json(recargasPOS);
        }


        // GET: /Recarga/Create
        /*public ActionResult Create()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["nom"] = Session["nom"].ToString();
                TempData["user"] = Session["user"].ToString();
                ViewBag.Panel = "nlightblue";
                ViewBag.Seguridad = "nlightblue";
                ViewBag.Servicios = "nlightblue";
                ViewBag.Estadisticas = "norange current open";
                ViewBag.idTarjeta = new SelectList(db.tarjetas, "idTarjeta", "serial");
                ViewBag.idUsuario = new SelectList(db.usuarios, "idUsuario", "apellido");
                ViewBag.idPos = new SelectList(db.pos, "idPOS", "nombre");
                ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion");
                return View();
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }*/

        // POST: /Recarga/Create
        /*[HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Recarga recarga)
        {
            if (ModelState.IsValid)
            {
                recarga.idRecarga = Guid.NewGuid();
                db.recargas.Add(recarga);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idTarjeta = new SelectList(db.tarjetas, "idTarjeta", "serial", recarga.idTarjeta);
            ViewBag.idUsuario = new SelectList(db.usuarios, "idUsuario", "apellido", recarga.idUsuario);
            ViewBag.idPos = new SelectList(db.pos, "idPOS", "nombre", recarga.idPos);
            ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion", recarga.idEstado);
            return View(recarga);
        }*/

        // GET: /Recarga/Edit/5
        /*public ActionResult Edit(Guid id)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["nom"] = Session["nom"].ToString();
                TempData["user"] = Session["user"].ToString();
                ViewBag.Panel = "nlightblue";
                ViewBag.Seguridad = "nlightblue";
                ViewBag.Servicios = "nlightblue";
                ViewBag.Estadisticas = "norange current open";
                Recarga recarga = db.recargas.Find(id);
                if (recarga == null)
                {
                    return HttpNotFound();
                }
                ViewBag.idTarjeta = new SelectList(db.tarjetas, "idTarjeta", "serial", recarga.idTarjeta);
                ViewBag.idUsuario = new SelectList(db.usuarios, "idUsuario", "apellido", recarga.idUsuario);
                ViewBag.idPos = new SelectList(db.pos, "idPOS", "nombre", recarga.idPos);
                ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion", recarga.idEstado);
                return View(recarga);
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }*/
        
        // POST: /Recarga/Edit/5
        /*[HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Recarga recarga)
        {
            if (ModelState.IsValid)
            {
                db.Entry(recarga).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idTarjeta = new SelectList(db.tarjetas, "idTarjeta", "serial", recarga.idTarjeta);
            ViewBag.idUsuario = new SelectList(db.usuarios, "idUsuario", "apellido", recarga.idUsuario);
            ViewBag.idPos = new SelectList(db.pos, "idPOS", "nombre", recarga.idPos);
            ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion", recarga.idEstado);
            return View(recarga);
        }*/
        
        // GET: /Recarga/Delete/5
        /*public ActionResult Delete(Guid id)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["nom"] = Session["nom"].ToString();
                TempData["user"] = Session["user"].ToString();
                ViewBag.Panel = "nlightblue";
                ViewBag.Seguridad = "nlightblue";
                ViewBag.Servicios = "nlightblue";
                ViewBag.Estadisticas = "norange current open";
                Recarga recarga = db.recargas.Find(id);
                if (recarga == null)
                {
                    return HttpNotFound();
                }
                return View(recarga);
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }*/
        
        // POST: /Recarga/Delete/5
        /*[HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Recarga recarga = db.recargas.Find(id);
            db.recargas.Remove(recarga);
            db.SaveChanges();
            return RedirectToAction("Index");
        }*/

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}