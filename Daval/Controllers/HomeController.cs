﻿using System.Web.Mvc;
using Daval.Controllers.DSTableAdapters;

namespace Daval.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
                TempData["nom"] = Session["nom"].ToString();
                TempData["user"] = Session["user"].ToString();
                ViewBag.Panel = "norange current";
                ViewBag.Seguridad = "nlightblue";
                ViewBag.Servicios = "nlightblue";
                ViewBag.Estadisticas = "nlightblue";
                //----------------------------------
                // Estadísticas
                PanelTableAdapter statsPanel = new PanelTableAdapter();
                if (statsPanel.ComprasHoy() != null)
                    ViewBag.ComprasHoy = (int)statsPanel.ComprasHoy();
                else 
                    ViewBag.ComprasHoy = 0;
                if(statsPanel.M3Hoy() != null)
                    ViewBag.M3Hoy = (int)statsPanel.M3Hoy();
                else
                    ViewBag.M3Hoy = 0;
                if (statsPanel.TotalCompras() != null)
                    ViewBag.ComprasT = (int)statsPanel.TotalCompras();
                else
                    ViewBag.ComprasT = 0;
                if (statsPanel.M3Vendidos() != null)
                    ViewBag.M3Total = (int)statsPanel.M3Vendidos();
                else
                    ViewBag.M3Total = 0;
                if (statsPanel.CantClientes() != null)
                    ViewBag.TotalClientes = (int)statsPanel.CantClientes();
                else
                    ViewBag.TotalClientes = 0;
                if (statsPanel.CantPOS() != null)
                    ViewBag.TotalPOS = (int)statsPanel.CantPOS();
                else
                    ViewBag.TotalPOS = 0;
                //----------------------------------
                return View();
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // GET: /Home/Restringido
        public ActionResult Restringido()
        {
            return View();
        }
    }
}