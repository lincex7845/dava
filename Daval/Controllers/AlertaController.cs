﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Daval.Models;

namespace Daval.Controllers
{
    public class AlertaController : Controller
    {
        private DavalDBContext db = new DavalDBContext();
        private Validacion v = Validacion.Instance;
        private const string opcion = "Historial de Alertas";
        //
        // GET: /Alerta/

        public ActionResult Index()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "nlightblue";
                    ViewBag.Estadisticas = "norange current open";
                    var alertas = db.alertas.Include(a => a.medidor).Include(a => a.tiposAlerta);
                    return View(alertas.ToList());
                }
                else
                    return RedirectToAction("Restringido", "Home");
            }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // GET: /Alerta/Details/5

        /*public ActionResult Details(int id = 0)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["nom"] = Session["nom"].ToString();
                TempData["user"] = Session["user"].ToString();
                ViewBag.Panel = "nlightblue";
                ViewBag.Seguridad = "nlightblue";
                ViewBag.Servicios = "nlightblue";
                ViewBag.Estadisticas = "norange current open";
                Alerta alerta = db.alertas.Find(id);
                if (alerta == null)
                {
                    return HttpNotFound();
                }
                return View(alerta);
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }*/

        //
        // GET: /Alerta/Create

        public ActionResult Create()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["nom"] = Session["nom"].ToString();
                TempData["user"] = Session["user"].ToString();
                ViewBag.Panel = "nlightblue";
                ViewBag.Seguridad = "nlightblue";
                ViewBag.Servicios = "nlightblue";
                ViewBag.Estadisticas = "norange current open";
                ViewBag.idMedidor = new SelectList(db.medidores, "idMedidor", "serial");
                ViewBag.idTipoAlerta = new SelectList(db.tipoAlertas, "idTipoAlerta", "descripcion");
                return View();
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /Alerta/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Alerta alerta)
        {
            if (ModelState.IsValid)
            {
                db.alertas.Add(alerta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idMedidor = new SelectList(db.medidores, "idMedidor", "serial", alerta.idMedidor);
            ViewBag.idTipoAlerta = new SelectList(db.tipoAlertas, "idTipoAlerta", "descripcion", alerta.idTipoAlerta);
            return View(alerta);
        }

        //
        // GET: /Alerta/Edit/5

        /*public ActionResult Edit(int id = 0)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["nom"] = Session["nom"].ToString();
                TempData["user"] = Session["user"].ToString();
                ViewBag.Panel = "nlightblue";
                ViewBag.Seguridad = "nlightblue";
                ViewBag.Servicios = "nlightblue";
                ViewBag.Estadisticas = "norange current open";
                Alerta alerta = db.alertas.Find(id);
                if (alerta == null)
                {
                    return HttpNotFound();
                }
                ViewBag.idMedidor = new SelectList(db.medidores, "idMedidor", "serial", alerta.idMedidor);
                ViewBag.idTipoAlerta = new SelectList(db.tipoAlertas, "idTipoAlerta", "descripcion", alerta.idTipoAlerta);
                return View(alerta);
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }*/

        //
        // POST: /Alerta/Edit/5

        /*[HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Alerta alerta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(alerta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idMedidor = new SelectList(db.medidores, "idMedidor", "serial", alerta.idMedidor);
            ViewBag.idTipoAlerta = new SelectList(db.tipoAlertas, "idTipoAlerta", "descripcion", alerta.idTipoAlerta);
            return View(alerta);
        }*/

        //
        // GET: /Alerta/Delete/5

        /*public ActionResult Delete(int id = 0)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["nom"] = Session["nom"].ToString();
                TempData["user"] = Session["user"].ToString();
                ViewBag.Panel = "nlightblue";
                ViewBag.Seguridad = "nlightblue";
                ViewBag.Servicios = "nlightblue";
                ViewBag.Estadisticas = "norange current open";
                Alerta alerta = db.alertas.Find(id);
                if (alerta == null)
                {
                    return HttpNotFound();
                }
                return View(alerta);
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }*/

        //
        // POST: /Alerta/Delete/5

        /*[HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Alerta alerta = db.alertas.Find(id);
            db.alertas.Remove(alerta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }*/

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}