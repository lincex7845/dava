﻿using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Daval.Models;
using System.Web;
using System.IO;
using System;
using CsvHelper;
using System.Collections.Generic;
using Daval.Controllers.DSTableAdapters;

namespace Daval.Controllers
{
    public class TarjetaController : Controller
    {
        private DavalDBContext db = new DavalDBContext();
        private Validacion v = Validacion.Instance;
        private const string opcion = "Gestión de Tarjetas";
        //
        // GET: /Tarjeta/

        public ActionResult Index()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {

                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    var tarjetas = db.tarjetas.Include(t => t.estado);
                    return View(tarjetas.ToList());
                }
                else
                    return RedirectToAction("Restringido", "Home");
            }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // GET: /Tarjeta/Details/5

        public ActionResult Details(int id)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {

                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    Tarjeta tarjeta = db.tarjetas.Find(id);
                    if (tarjeta == null)
                    {
                        return HttpNotFound();
                    }
                    return View(tarjeta);
                }
                else 
                    return RedirectToAction("Restringido", "Home");
            }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // GET: /Tarjeta/Create

        public ActionResult Create()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {

                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion");
                    return View();
                }
                else 
                    return RedirectToAction("Restringido", "Home");
            }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /Tarjeta/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tarjeta tarjeta)
        {
            if (ModelState.IsValid)
            {
                db.tarjetas.Add(tarjeta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion", tarjeta.idEstado);
            return View(tarjeta);
        }

        //
        // GET: /Tarjeta/Edit/5

        public ActionResult Edit(int id)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {

                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    Tarjeta tarjeta = db.tarjetas.Find(id);
                    if (tarjeta == null)
                    {
                        return HttpNotFound();
                    }
                    ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion", tarjeta.idEstado);
                    return View(tarjeta);
                }
                else 
                    return RedirectToAction("Restringido", "Home");
            }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /Tarjeta/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tarjeta tarjeta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tarjeta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion", tarjeta.idEstado);
            return View(tarjeta);
        }

        //
        // GET: /Tarjeta/Delete/5

        public ActionResult Delete(int id)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {

                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    Tarjeta tarjeta = db.tarjetas.Find(id);
                    if (tarjeta == null)
                    {
                        return HttpNotFound();
                    }
                    return View(tarjeta);
                }
                else 
                    return RedirectToAction("Restringido", "Home");
            }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /Tarjeta/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tarjeta tarjeta = db.tarjetas.Find(id);
            db.tarjetas.Remove(tarjeta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //
        // GET: /Tarjeta/Add
        public ActionResult Add()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    return View();
                }
                else
                    return RedirectToAction("Restringido", "Home");
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /Tarjeta/Add
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Add(HttpPostedFileWrapper listaTarjetas)
        {
            if (listaTarjetas == null || listaTarjetas.ContentLength == 0)
            {
                ModelState.AddModelError("", "No se ha seleccionado un archivo");
                TempData["nom"] = Session["nom"].ToString();
                TempData["user"] = Session["user"].ToString();
                ViewBag.Panel = "nlightblue";
                ViewBag.Seguridad = "nlightblue";
                ViewBag.Servicios = "norange current open";
                ViewBag.Estadisticas = "nlightblue";
                return View(listaTarjetas);
            }
            else
            {

                var extension = Path.GetExtension(listaTarjetas.FileName);
                if (extension.Equals(".csv"))
                {
                    StreamReader sr = null;
                    try
                    {
                        var archivo = string.Format("{0}", Guid.NewGuid().ToString() + "-" + Path.GetFileName(listaTarjetas.FileName));
                        var ruta = Path.Combine(Server.MapPath(Url.Content("~/ArchivosRegistro")), archivo);
                        listaTarjetas.SaveAs(ruta);
                        sr = new StreamReader(ruta);
                        CsvReader csvReader = new CsvReader(sr);
                        IEnumerable<RegistrarTarjetas> records = csvReader.GetRecords<RegistrarTarjetas>();
                        RegistrarTarjetasTableAdapter RTTA = new RegistrarTarjetasTableAdapter();
                        DS.RegistrarTarjetasDataTable RTDT = new DS.RegistrarTarjetasDataTable();
                        foreach (var r in records) // Each record will be fetched and printed on the screen
                        {
                            RTDT.Clear();
                            RTTA.Fill(RTDT, r.serial, r.lote, r.contrasena, r.fecha_entrada, r.fecha_salida, r.idEstado);
                            foreach (DS.RegistrarTarjetasRow x in RTDT.Rows)
                                if (x.resultado != 200)
                                {
                                    switch (x.resultado)
                                    {
                                        case 300:
                                            ModelState.AddModelError("", "Ocurrió un problema al guardar los datos.");
                                            break;
                                        case 500:
                                            ModelState.AddModelError("", "Existe una inconsistencia en los datos a guardar: " + x.ErrorMessage);
                                            break;
                                    }
                                    sr.Close();
                                    TempData["nom"] = Session["nom"].ToString();
                                    TempData["user"] = Session["user"].ToString();
                                    ViewBag.Panel = "nlightblue";
                                    ViewBag.Seguridad = "nlightblue";
                                    ViewBag.Servicios = "norange current open";
                                    ViewBag.Estadisticas = "nlightblue";
                                    return View(listaTarjetas);
                                }
                        }
                        sr.Close();
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", "Existe una inconsistencia en los datos a guardar: " + e.Message);
                        sr.Close();
                        TempData["nom"] = Session["nom"].ToString();
                        TempData["user"] = Session["user"].ToString();
                        ViewBag.Panel = "nlightblue";
                        ViewBag.Seguridad = "nlightblue";
                        ViewBag.Servicios = "norange current open";
                        ViewBag.Estadisticas = "nlightblue";
                        return View(listaTarjetas);
                    }

                    return RedirectToAction("Index", "Tarjeta");
                }
                else
                {
                    ModelState.AddModelError("", "No se ha seleccionado un archivo válido");
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    return View(listaTarjetas);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}