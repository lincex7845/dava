﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Daval.Models;
using Daval.Controllers.DSTableAdapters;

namespace Daval.Controllers
{
    
    public class SesionController : Controller
    {
        private DavalDBContext db = new DavalDBContext();

        //
        // GET: /Sesion/IniciarSesion
        [AllowAnonymous]
        public ActionResult IniciarSesion()
        {
            if (Session["user"] == null)
                return View();
            else
            {
                return RedirectToAction("Index", "Home");
            } 
        }

        //
        // POST: /Sesion/IniciarSesion
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult IniciarSesion(IniciarSesionModelo modelo)
        {
            if (ModelState.IsValid)
            {
                //Usuario aux = null;
                //aux = (from usu in db.usuarios 
                //       where usu.usuario == modelo.usuario
                //       && usu.contraseña == modelo.pass
                //       select usu).FirstOrDefault();
                PanelTableAdapter fun = new PanelTableAdapter();
                int c = (int)fun.ValidarUsuario(modelo.usuario, modelo.pass);
                if (c == 1)
                {
                    Usuario aux = null;
                    aux = (from usu in db.usuarios
                           where usu.usuario == modelo.usuario
                           && usu.contraseña == modelo.pass
                           select usu).FirstOrDefault();
                    Session["user"] = aux.usuario;
                    Session["nom"] = aux.nombre + " " + aux.apellido;
                    Session["rol"] = aux.rol.rolName;
                    TempData["user"] = Session["user"].ToString();
                    //this.CrearCokkie();
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    // Usuario no registrado
                    ModelState.AddModelError("", "El usuario no está registrado.");
                    return View(modelo);
                }
            }

            // Error
            ModelState.AddModelError("", "El usuario o la contraseña digitados son incorrectos.");
            return View(modelo);
        }

        //private void CrearCokkie()
        //{
        //    string usuarioKey = TempData["user"].ToString() + "configuracion";
        //    HttpCookie cookie = Request.Cookies[usuarioKey];
        //    if (cookie == null)
        //    {
        //        cookie = new HttpCookie(usuarioKey);
        //        cookie["RE"] = "5500";
        //        cookie["CS"] = "60";
        //        cookie["NT"] = "60";
        //        cookie["MT"] = "60";
        //        cookie.Expires = DateTime.Now.AddDays(7);
        //        Response.Cookies.Add(cookie);
        //    }
        //}

        // POST: /Sesion/CerrarSesion
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CerrarSesion()
        {
            Session.Abandon();
            Session.RemoveAll();
            return RedirectToAction("IniciarSesion", "Sesion");
        }

        ////
        //// GET: /Sesion/Registrar
        //[AllowAnonymous]
        //public ActionResult Registrar()
        //{
        //    return View();
        //}

        ////
        //// POST: /Sesion/Registrar
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public ActionResult Registrar(RegistrarModelo modelo)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            //int x = (from uxu in db.usuarios
        //            //         where uxu.usuario == modelo.usuario.Trim()
        //            //         select uxu).Count();
        //            PanelTableAdapter fun = new PanelTableAdapter();
        //            int c = (int)fun.ValidarUsuario(modelo.usuario, modelo.pass);
        //            if (c == 0)
        //            {
        //                var aux = new Usuario
        //                {
        //                    apellido = "ape usuario",
        //                    nombre = "nom usuario",
        //                    usuario = modelo.usuario,
        //                    contraseña = modelo.pass,
        //                    idRol = 3,
        //                    idEstado = 1
        //                };
        //                db.usuarios.Add(aux);
        //                db.SaveChanges();
        //                Session["user"] = aux.usuario;
        //                Session["nom"] = aux.nombre + " " + aux.apellido;
        //                Session["rol"] = db.roles.Find(aux.idRol).rolName.ToString();
        //                //this.CrearCokkie();
        //                return RedirectToAction("Index", "Home");
        //            }
        //            else
        //                ModelState.AddModelError("", "El usuario ya se encuentra registrado");
        //        }
        //        catch(Exception e)
        //        {
        //            ModelState.AddModelError("", e.Message);
        //        }
        //    }
            
        //    return View(modelo);
        //}

        //
        // GET: /Sesion/Perfil
        public ActionResult Perfil()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["nom"] = Session["nom"].ToString();
                TempData["user"] = Session["user"].ToString();
                ViewBag.Panel = "norange current";
                ViewBag.Seguridad = "nlightblue";
                ViewBag.Servicios = "nlightblue";
                ViewBag.Estadisticas = "nlightblue";
                string user = TempData["user"].ToString();
                Usuario usuario = (from usu in db.usuarios
                                    where usu.usuario == user
                                    select usu).FirstOrDefault();
                if (usuario == null)
                {
                    return HttpNotFound();
                }

                var perfil = new PerfilModelo
                {
                    idUsuario = usuario.idUsuario,
                    idRol = usuario.idRol,
                    idEstado = usuario.idEstado,
                    nombre = usuario.nombre,
                    apellido = usuario.apellido,
                    usuario = usuario.usuario,
                    contraseña = usuario.contraseña,
                    passActual = usuario.contraseña,
                    passNueva = "",
                    confirmarPass = ""
                };

                return View(perfil);
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /Sesion/Perfil
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Perfil(PerfilModelo modelo)
        {
            if (ModelState.IsValid)
            {
                Usuario aux = db.usuarios.Find(modelo.idUsuario);
                aux.apellido = modelo.apellido;
                aux.nombre = modelo.nombre;
                aux.contraseña = modelo.passNueva;
                db.Entry(aux).State = EntityState.Modified;
                db.SaveChanges();
                Session["user"] = aux.usuario;
                Session["nom"] = aux.nombre + " " + aux.apellido;
                return RedirectToAction("Index", "Home");
            }
            return View(modelo);
        }

        //
        //GET: /Sesion/Configuracion
        //public ActionResult Configuracion()
        //{
        //    if (Session["user"] != null && Session["nom"] != null)
        //    {
        //        TempData["nom"] = Session["nom"].ToString();
        //        TempData["user"] = Session["user"].ToString();
        //        ViewBag.Panel = "norange current";
        //        ViewBag.Seguridad = "nlightblue";
        //        ViewBag.Servicios = "nlightblue";
        //        ViewBag.Estadisticas = "nlightblue";

        //        Configuraciones config = new Configuraciones();
        //        HttpCookie c =   Request.Cookies[TempData["user"].ToString() + "configuracion"];
        //        config.recargaEmergencia = int.Parse(c["RE"]);
        //        config.intervaloCopiaSeguridad = int.Parse(c["CS"]);
        //        config.intervaloNotificacion = int.Parse(c["NT"]);
        //        config.intervaloMantenimiento = int.Parse(c["MT"]);
        //        return View(config);
        //    }
        //    else 
        //        return RedirectToAction("IniciarSesion", "Sesion");
        //}

        //
        //POST: /Sesion/Configuracion
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Configuracion(Configuraciones modelo)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        TempData["user"] = Session["user"].ToString();
        //        HttpCookie c = Request.Cookies[TempData["user"].ToString() + "configuracion"];
        //        c["RE"] = modelo.recargaEmergencia.ToString();
        //        c["CS"] = modelo.intervaloCopiaSeguridad.ToString();
        //        c["NT"] = modelo.intervaloNotificacion.ToString();
        //        c["MT"] = modelo.intervaloMantenimiento.ToString();
        //        c.Expires = DateTime.Now.AddDays(7);
        //        Response.Cookies.Add(c);
        //        return RedirectToAction("Index", "Home");
        //    }
        //    return View();
        //}
    }
}