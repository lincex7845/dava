﻿using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Daval.Models;

namespace Daval.Controllers
{
    public class UsuarioController : Controller
    {
        private DavalDBContext db = new DavalDBContext();
        private Validacion v = Validacion.Instance;
        private const string opcion = "Gestión de Usuarios";
        //
        // GET: /Usuario/Index

        public ActionResult Index()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "norange current open";
                    ViewBag.Servicios = "nlightblue";
                    ViewBag.Estadisticas = "nlightblue";
                    var usuarios = db.usuarios.Include(u => u.rol).Include(u => u.estado);
                    return View(usuarios.ToList());
                }
                else
                    return RedirectToAction("Restringido", "Home");
            }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // GET: /Usuario/Details/5

        public ActionResult Details(int id = 0)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "norange current open";
                    ViewBag.Servicios = "nlightblue";
                    ViewBag.Estadisticas = "nlightblue";
                    Usuario usuario = db.usuarios.Find(id);
                    if (usuario == null)
                    {
                        return HttpNotFound();
                    }
                    return View(usuario);
                }
                else 
                    return RedirectToAction("Restringido", "Home");
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // GET: /Usuario/Create

        public ActionResult Create()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                 TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "norange current open";
                    ViewBag.Servicios = "nlightblue";
                    ViewBag.Estadisticas = "nlightblue";
                    ViewBag.idRol = new SelectList(db.roles, "idRol", "rolName");
                    ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion");
                    return View();
                }
                else 
                    return RedirectToAction("Restringido", "Home");
            }
             else
                 return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /Usuario/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Usuario usuarioNuevo)
        {
            if (ModelState.IsValid)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (TempData["rol"].ToString().Equals("Supervisor") &&
                    usuarioNuevo.idRol == 3)
                {
                    db.usuarios.Add(usuarioNuevo);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else if ( TempData["rol"].ToString().Equals("Tecnico de soporte") &&
                          ( usuarioNuevo.idRol == 5 ||
                            usuarioNuevo.idRol == 6    )
                        )
                {
                    db.usuarios.Add(usuarioNuevo);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else if(TempData["rol"].ToString().Equals("Superusuario"))
                {
                    db.usuarios.Add(usuarioNuevo);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                    return RedirectToAction("Restringido", "Home");
            }

            ViewBag.idRol = new SelectList(db.roles, "idRol", "rolName", usuarioNuevo.idRol);
            ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion", usuarioNuevo.idEstado);
            return View(usuarioNuevo);
        }

        //
        // GET: /Usuario/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                 TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "norange current open";
                    ViewBag.Servicios = "nlightblue";
                    ViewBag.Estadisticas = "nlightblue";
                    Usuario usuario = db.usuarios.Find(id);
                    if (usuario == null)
                    {
                        return HttpNotFound();
                    }
                    ViewBag.idRol = new SelectList(db.roles, "idRol", "rolName", usuario.idRol);
                    //ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion", usuario.idEstado);
                    return View(usuario);
                }
                else 
                    return RedirectToAction("Restringido", "Home");
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /Usuario/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Usuario usuarioEdit)
        {
            if (ModelState.IsValid)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (TempData["rol"].ToString().Equals("Supervisor") &&
                    usuarioEdit.idRol == 3)
                {
                    db.Entry(usuarioEdit).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else if (TempData["rol"].ToString().Equals("Tecnico de soporte") &&
                          (usuarioEdit.idRol == 5 ||
                            usuarioEdit.idRol == 6)
                        )
                {
                    db.Entry(usuarioEdit).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else if (TempData["rol"].ToString().Equals("Superusuario"))
                {
                    db.Entry(usuarioEdit).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                    return RedirectToAction("Restringido", "Home");
                //Usuario aux = db.usuarios.Find(usuarioEdit.idUsuario);
                //usuarioEdit.idEstado = aux.idEstado;
                
            }
            ViewBag.idRol = new SelectList(db.roles, "idRol", "rolName", usuarioEdit.idRol);
            //ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion", usuario.idEstado);
            return View(usuarioEdit);
        }

        //
        // GET: /Usuario/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                 TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "norange current open";
                    ViewBag.Servicios = "nlightblue";
                    ViewBag.Estadisticas = "nlightblue";                
                    Usuario usuario = db.usuarios.Find(id);
                    if (usuario == null)
                    {
                        return HttpNotFound();
                    }
                    return View(usuario);
                }
                else 
                    return RedirectToAction("Restringido", "Home");
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /Usuario/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Usuario usuario = db.usuarios.Find(id);
            db.usuarios.Remove(usuario);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}