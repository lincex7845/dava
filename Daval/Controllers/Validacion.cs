﻿using Daval.Controllers.DSTableAdapters;

namespace Daval.Controllers
{
    public class Validacion
    {
        private static Validacion instance;

        public static Validacion Instance
        {
            get 
            {
                if (instance == null)
                    instance = new Validacion();
                return Validacion.instance; 
            }
        }

        // VALIDAR OPCIONES PARA EL USUARIO
        public bool ValidarOpcion(string rol, string opcion)
        {
            bool b = false;
            DS.OpcionByRolDataTable ORDT = new DS.OpcionByRolDataTable();
            OpcionByRolTableAdapter ORTA = new OpcionByRolTableAdapter();
            ORDT.Clear();
            ORTA.Fill(ORDT, rol);
            foreach (DS.OpcionByRolRow r in ORDT.Rows)
            {
                if (r.nombreOpcion.ToString().Equals(opcion))
                {
                    b = true;
                    break;
                }
            }
            return b;
        } 
    }
}