﻿using System.Data;
using System.Linq;
using System.Web.Mvc;
using Daval.Models;

namespace Daval.Controllers
{
    public class MunicipioController : Controller
    {
        private DavalDBContext db = new DavalDBContext();
        private Validacion v = Validacion.Instance;
        private const string opcion = "Gestión de Municipios";
        //
        // GET: /Municipio/

        public ActionResult Index()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    return View(db.municipios.ToList());
                }
                else
                    return RedirectToAction("Restringido", "Home");
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // GET: /Municipio/Details/5
        //public ActionResult Details(int id = 0)
        //{
        //    if (Session["user"] != null && Session["nom"] != null)
        //    {
        //        TempData["nom"] = Session["nom"].ToString();
        //        TempData["user"] = Session["user"].ToString();
        //        ViewBag.Panel = "nlightblue";
        //        ViewBag.Seguridad = "nlightblue";
        //        ViewBag.Servicios = "norange current open";
        //        ViewBag.Estadisticas = "nlightblue";
        //        Municipio municipio = db.municipios.Find(id);
        //        if (municipio == null)
        //        {
        //            return HttpNotFound();
        //        }
        //        return View(municipio);
        //     }
        //    else 
        //        return RedirectToAction("IniciarSesion", "Sesion");
        //}

        //
        // GET: /Municipio/Create

        public ActionResult Create()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    return View();
                }
                else 
                    return RedirectToAction("Restringido", "Home");
             }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");

        }

        //
        // POST: /Municipio/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Municipio municipio)
        {
            if (ModelState.IsValid)
            {
                db.municipios.Add(municipio);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(municipio);
        }

        //
        // GET: /Municipio/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["nom"] = Session["nom"].ToString();
                TempData["user"] = Session["user"].ToString();
                ViewBag.Panel = "nlightblue";
                ViewBag.Seguridad = "nlightblue";
                ViewBag.Servicios = "norange current open";
                ViewBag.Estadisticas = "nlightblue";
                Municipio municipio = db.municipios.Find(id);
                if (municipio == null)
                {
                    return HttpNotFound();
                }
                return View(municipio);
             }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");

        }

        //
        // POST: /Municipio/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Municipio municipio)
        {
            if (ModelState.IsValid)
            {
                db.Entry(municipio).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(municipio);
        }

        //
        // GET: /Municipio/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    Municipio municipio = db.municipios.Find(id);
                    if (municipio == null)
                    {
                        return HttpNotFound();
                    }
                    return View(municipio);
                }
                else 
                    return RedirectToAction("Restringido", "Home");
             }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /Municipio/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Municipio municipio = db.municipios.Find(id);
            db.municipios.Remove(municipio);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}