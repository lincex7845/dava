﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Daval.Models;
using System.IO;
using System.Collections.Generic;
using Daval.Controllers.DSTableAdapters;
using CsvHelper;

namespace Daval.Controllers
{
    public class MedidorController : Controller
    {
        private DavalDBContext db = new DavalDBContext();
        private Validacion v = Validacion.Instance;
        private const string opcion = "Gestión de Medidores";
        //
        // GET: /Medidor/

        public ActionResult Index()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    var medidores = db.medidores.Include(m => m.estados);
                    return View(medidores.ToList());
                }
                else
                    return RedirectToAction("Restringido", "Home");
             }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // GET: /Medidor/Details/5

        public ActionResult Details(int id = 0)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    Medidor medidor = db.medidores.Find(id);
                    if (medidor == null)
                    {
                        return HttpNotFound();
                    }
                    return View(medidor);
                }
                else 
                    return RedirectToAction("Restringido", "Home");
            }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // GET: /Medidor/Create

        public ActionResult Create()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion");
                    return View();
                }
                else 
                    return RedirectToAction("Restringido", "Home");
            }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /Medidor/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Medidor medidorNuevo)
        {
            if (ModelState.IsValid)
            {
                db.medidores.Add(medidorNuevo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion", medidorNuevo.idEstado);
            return View(medidorNuevo);
        }

        //
        // GET: /Medidor/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    Medidor medidor = db.medidores.Find(id);
                    if (medidor == null)
                    {
                        return HttpNotFound();
                    }
                    ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion", medidor.idEstado);
                    return View(medidor);
                }
                else 
                    return RedirectToAction("Restringido", "Home");
            }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /Medidor/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Medidor medidorEdit)
        {
            if (ModelState.IsValid)
            {
                db.Entry(medidorEdit).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idEstado = new SelectList(db.estados, "idEstado", "descripcion", medidorEdit.idEstado);
            return View(medidorEdit);
        }

        //
        // GET: /Medidor/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    Medidor medidor = db.medidores.Find(id);
                    if (medidor == null)
                    {
                        return HttpNotFound();
                    }
                    return View(medidor);
                }
                else 
                    return RedirectToAction("Restringido", "Home");
            }
            else 
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /Medidor/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Medidor medidor = db.medidores.Find(id);
            db.medidores.Remove(medidor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //
        // GET: /Medidor/Add
        public ActionResult Add()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    return View();
                }
                else
                    return RedirectToAction("Restringido", "Home");
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /Medidor/Add
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Add(HttpPostedFileWrapper listaMedidores)
        {
            if (listaMedidores == null || listaMedidores.ContentLength == 0)
            {
                ModelState.AddModelError("", "No se ha seleccionado un archivo");
                TempData["nom"] = Session["nom"].ToString();
                TempData["user"] = Session["user"].ToString();
                ViewBag.Panel = "nlightblue";
                ViewBag.Seguridad = "nlightblue";
                ViewBag.Servicios = "norange current open";
                ViewBag.Estadisticas = "nlightblue";
                return View(listaMedidores);
            }
            else
            {
                
                var extension = Path.GetExtension(listaMedidores.FileName);
                if (extension.Equals(".csv"))
                {
                    StreamReader sr = null;
                    try
                    {
                        var archivo = string.Format("{0}", Guid.NewGuid().ToString() + "-" + Path.GetFileName(listaMedidores.FileName)); 
                        var ruta = Path.Combine(Server.MapPath(Url.Content("~/ArchivosRegistro")), archivo);
                        listaMedidores.SaveAs(ruta);
                        sr = new StreamReader(ruta);
                        CsvReader csvReader = new CsvReader(sr);
                        IEnumerable<RegistrarMedidores> records = csvReader.GetRecords<RegistrarMedidores>();
                        RegistrarMedidoresTableAdapter RMTA = new RegistrarMedidoresTableAdapter();
                        DS.RegistrarMedidoresDataTable RMDT = new DS.RegistrarMedidoresDataTable();
                        foreach (var r in records) // Each record will be fetched and printed on the screen
                        {
                            RMDT.Clear();
                            RMTA.FillInsertarMedidores(RMDT, r.serial, r.lote, r.fecha_entrada, r.fecha_salida, r.mca, r.mcc, r.nivelBateria, r.idEstado);
                            foreach(DS.RegistrarMedidoresRow x in RMDT.Rows)
                                if (x.resultado != 200)
                                {
                                    switch (x.resultado)
                                    {
                                        case 300:
                                            ModelState.AddModelError("", "Ocurrió un problema al guardar los datos.");
                                            break;
                                        case 500:
                                            ModelState.AddModelError("", "Existe una inconsistencia en los datos a guardar: " + x.ErrorMessage);
                                            break;
                                    } 
                                    sr.Close();
                                    TempData["nom"] = Session["nom"].ToString();
                                    TempData["user"] = Session["user"].ToString();
                                    ViewBag.Panel = "nlightblue";
                                    ViewBag.Seguridad = "nlightblue";
                                    ViewBag.Servicios = "norange current open";
                                    ViewBag.Estadisticas = "nlightblue";
                                    return View(listaMedidores);
                                }
                        }
                        sr.Close();
                        //using (var fileReader = System.IO.File.OpenText(ruta))
                        //using (var csvReader = new CsvHelper.CsvReader(fileReader))
                        //{
                        //    while (csvReader.Read())
                        //    {
                        //        var r = csvReader.GetRecord<RegistrarMedidores>();
                        //        res = new RegistrarMedidoresTableAdapter().Fill(
                        //        new DS.RegistrarMedidoresDataTable(),
                        //        r.serial,
                        //        r.lote,
                        //        r.fecha_entrada,
                        //        r.fecha_salida,
                        //        r.mca,
                        //        r.mcc,
                        //        r.nivelBateria,
                        //        r.idEstado);
                        //        if (res != 200)
                        //        {
                        //            ModelState.AddModelError("", "pailas");
                        //            return View(listaMedidores);
                        //        }
                        //    }
                        //    fileReader.Close();
                        //}
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", "Existe una inconsistencia en los datos a guardar: " + e.Message);
                        sr.Close();
                        TempData["nom"] = Session["nom"].ToString();
                        TempData["user"] = Session["user"].ToString();
                        ViewBag.Panel = "nlightblue";
                        ViewBag.Seguridad = "nlightblue";
                        ViewBag.Servicios = "norange current open";
                        ViewBag.Estadisticas = "nlightblue";
                        return View(listaMedidores);
                    }

                    return RedirectToAction("Index", "Medidor");
                }
                else
                {
                    ModelState.AddModelError("", "No se ha seleccionado un archivo válido");
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "norange current open";
                    ViewBag.Estadisticas = "nlightblue";
                    return View(listaMedidores);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}