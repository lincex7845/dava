﻿using System.Data;
using System.Web.Mvc;
using Daval.Models;

namespace Daval.Controllers
{
    public class ConfiguracionesController : Controller
    {
        private DavalDBContext db = new DavalDBContext();
        private Validacion v = Validacion.Instance;
        private const string opcion = "Gestión de Configuraciones";

        //
        // GET: /Configuraciones/
        public ActionResult Index()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "norange current";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "nlightblue";
                    ViewBag.Estadisticas = "nlightblue";
                    Configuraciones configuraciones = db.configuraciones.Find(1);
                    if (configuraciones == null)
                    {
                        return HttpNotFound();
                    }
                    return View(configuraciones);
                }
                else
                    return RedirectToAction("Restringido", "Home");
            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // POST: /Configuraciones/
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(Configuraciones configuraciones)
        {
            if (ModelState.IsValid)
            {
                db.Entry(configuraciones).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(configuraciones);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}