﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Daval.Models;

namespace Daval.Controllers
{
    public class ClienteMedidorController : Controller
    {
        private DavalDBContext db = new DavalDBContext();
        private Validacion v = Validacion.Instance;
        private const string opcion = "Relación de Clientes y Medidores";
        //
        // GET: /ClienteMedidor/

        public ActionResult Index()
        {
            if (Session["user"] != null && Session["nom"] != null)
            {
                TempData["rol"] = Session["rol"].ToString();
                if (this.v.ValidarOpcion(TempData["rol"].ToString(), opcion))
                {
                    TempData["nom"] = Session["nom"].ToString();
                    TempData["user"] = Session["user"].ToString();
                    ViewBag.Panel = "nlightblue";
                    ViewBag.Seguridad = "nlightblue";
                    ViewBag.Servicios = "nlightblue";
                    ViewBag.Estadisticas = "norange current open";
                    var clientemedidor = db.clienteMedidor.Include(c => c.cliente).Include(c => c.medidor);
                    return View(clientemedidor.ToList());
                }
                else
                    return RedirectToAction("Restringido", "Home");

            }
            else
                return RedirectToAction("IniciarSesion", "Sesion");
        }

        //
        // GET: /ClienteMedidor/Details/5

        /*public ActionResult Details(string id = null)
        {
            ClienteMedidor clientemedidor = db.clienteMedidor.Find(id);
            if (clientemedidor == null)
            {
                return HttpNotFound();
            }
            return View(clientemedidor);
        }*/

        //
        // GET: /ClienteMedidor/Create

        /*public ActionResult Create()
        {
            ViewBag.cedula = new SelectList(db.clientes, "cedula", "apellido");
            ViewBag.idMedidor = new SelectList(db.medidores, "idMedidor", "serial");
            return View();
        }*/

        //
        // POST: /ClienteMedidor/Create

        /*[HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ClienteMedidor clientemedidor)
        {
            if (ModelState.IsValid)
            {
                db.clienteMedidor.Add(clientemedidor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.cedula = new SelectList(db.clientes, "cedula", "apellido", clientemedidor.cedula);
            ViewBag.idMedidor = new SelectList(db.medidores, "idMedidor", "serial", clientemedidor.idMedidor);
            return View(clientemedidor);
        }*/

        //
        // GET: /ClienteMedidor/Edit/5

        /*public ActionResult Edit(string id = null)
        {
            ClienteMedidor clientemedidor = db.clienteMedidor.Find(id);
            if (clientemedidor == null)
            {
                return HttpNotFound();
            }
            ViewBag.cedula = new SelectList(db.clientes, "cedula", "apellido", clientemedidor.cedula);
            ViewBag.idMedidor = new SelectList(db.medidores, "idMedidor", "serial", clientemedidor.idMedidor);
            return View(clientemedidor);
        }*/

        //
        // POST: /ClienteMedidor/Edit/5

        /*[HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ClienteMedidor clientemedidor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(clientemedidor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.cedula = new SelectList(db.clientes, "cedula", "apellido", clientemedidor.cedula);
            ViewBag.idMedidor = new SelectList(db.medidores, "idMedidor", "serial", clientemedidor.idMedidor);
            return View(clientemedidor);
        }*/

        //
        // GET: /ClienteMedidor/Delete/5

        /*public ActionResult Delete(string id = null)
        {
            ClienteMedidor clientemedidor = db.clienteMedidor.Find(id);
            if (clientemedidor == null)
            {
                return HttpNotFound();
            }
            return View(clientemedidor);
        }*/

        //
        // POST: /ClienteMedidor/Delete/5
        
        /*[HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ClienteMedidor clientemedidor = db.clienteMedidor.Find(id);
            db.clienteMedidor.Remove(clientemedidor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }*/

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}